package com.trancis.wall.java_class;

import android.graphics.Bitmap;

/**
 * Created by mohit on 03-07-2017.
 */
public class DataInfoCat {
    int cid;
    public int total;

    private String thumbUrl;
    String realUrl;
    public String catname;
    public Bitmap bitmap;
    public boolean isDownloading;

    public DataInfoCat(int id, String tUrl, String rUrl, String category_name, int totatcat) {
        cid = id;
        thumbUrl = tUrl;
        realUrl = rUrl;
        catname = category_name;
        total = totatcat;
    }

    public DataInfoCat(int j) {
        total = j;

    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public String getCategoryname() {
        return catname;
    }

    public Integer getId() {
        return cid;
    }

    public Integer getTotal() {
        return total;
    }

    public String getRealUrl() {
        return realUrl;
    }
}

