package com.trancis.wall.java_class;

import android.graphics.Bitmap;

/**
 * Created by mohit on 29-06-2017.
 */
public class DataInfoHome {
    int id;
    private String thumbUrl;
     String realUrl;
    public Bitmap bitmap;
    public boolean isDownloading;

    public DataInfoHome(int cid, String tUrl, String rUrl) {
        id = cid;
        thumbUrl = tUrl;
        realUrl = rUrl;
    }

    public String getThumbUrl() {
        return  thumbUrl;
    }




    public String getRealUrl() {
        return  realUrl;
    }
}
