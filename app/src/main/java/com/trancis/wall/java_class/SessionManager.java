package com.trancis.wall.java_class;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;


import com.trancis.wall.activity.Main2Activity;

import java.net.URI;

/**
 * Created by mohit on 12-06-2017.
 */
public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "AndroidSharedPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    public static final String FG_LOGIN = "googlefblogin";


    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "emailid";

    //setting profile image
    public static final String PROFILE_IMAGE = "uri";


    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     **/

    public void setLogin(boolean bool)
    {
         SharedPreferences.Editor editor = pref.edit();
         editor.putBoolean(IS_LOGIN, bool);
         editor.commit();
    }

    public void createLoginSession(String emailid) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing email in pref
        editor.putString(KEY_EMAIL, emailid);


        // commit changes
        editor.commit();
    }
    public void setChangepassword(String googlefblogin) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing email in pref
        editor.putString(FG_LOGIN, googlefblogin);


        // commit changes
        editor.commit();
    }

    public void createProfileImage(Uri uri)
    {
        editor.putString(PROFILE_IMAGE, uri.toString());
        // commit changes
        editor.commit();
    }
    /**
     * Check login method will check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */

    public boolean checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, Main2Activity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
            return false;
        }
        return true;
    }

    public String changepassword() {

        String hide = pref.getString(FG_LOGIN, null);

        // return user
        return hide;
    }

    /**
     * Get stored session data
     */

    public String getUserDetails() {

        // user email id
        String key = pref.getString(KEY_EMAIL, null);

        // return user
        return key;
    }

    public String getProfileImage() {

        // user email id
        String uri = pref.getString(PROFILE_IMAGE, null);

        // return user
        return uri;
    }

    /**
     * Clear session details
     */

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i9 = new Intent(_context, Main2Activity.class);

        // Closing all the Activities
        i9.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i9.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity

        _context.startActivity(i9);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn()
    {
        return pref.getBoolean(IS_LOGIN, false);
    }
}
