package com.trancis.wall.java_class;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by mohit on 06-07-2017.
 */
public class CheckNetwork extends AppCompatActivity
{

    Context _context;

    public CheckNetwork(Context context)
    {
        this._context = context;
    }



    public boolean isOnline() {
        try {
            ConnectivityManager conMgr = (ConnectivityManager) _context.getSystemService(this.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

            if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(_context);
                builder.setTitle("OOPS");
                builder.setMessage("No Internet Connection")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                ;
                AlertDialog alert = builder.create();
                alert.show();


            }
            return true;
        } catch (Exception e) {

        }
        return false;
    }
}
