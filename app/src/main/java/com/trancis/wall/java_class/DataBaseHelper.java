package com.trancis.wall.java_class;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mohit on 07-06-2017.
 */
public class DataBaseHelper extends SQLiteOpenHelper
{
    public static final String db_name="HD_WALL.db";
    public static final int db_version=1;

    public DataBaseHelper(Context context)
    {
        super(context, db_name, null, db_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String q="create table Login (fname text,lname text,emailid text,password text)";
        db.execSQL(q);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
