package com.trancis.wall.java_class;

import android.graphics.Bitmap;

/**
 * Created by trancis on 7/9/17.
 */

public class BitmapData {
    public String url;
    public Bitmap bitmap;

    public BitmapData(String url, Bitmap bitmap) {
        this.url = url;
        this.bitmap = bitmap;
    }
}
