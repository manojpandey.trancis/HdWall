package com.trancis.wall.java_class;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by trancis on 25/8/17.
 */

public class MediaModel implements Serializable {
    public String url = null;
    public long modifiedDate;
    public boolean isLoading;
    public Bitmap bitmap;

    public MediaModel(String url, long modifiedDate) {
        this.url = url;
        this.modifiedDate = modifiedDate;

    }
   /* public MediaModel(Parcel parcel) {
        this.url = parcel.readString();
        this.modifiedDate = modifiedDate;

    }
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<MediaModel> CREATOR = new Parcelable.Creator<MediaModel>() {

        @Override
        public MediaModel createFromParcel(Parcel source) {
            return new MediaModel(source);
        }

        @Override
        public MediaModel[] newArray(int size) {
            return new MediaModel[size];
        }
    };
    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
        parcel.writeLong(modifiedDate);

    }*/
}
