package com.trancis.wall.gcm;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by trancis on 6/9/17.
 */

public class GcmService extends GcmListenerService{
    public GcmService() {

    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d("Received GCM Message: ", data + "");
    }
}
