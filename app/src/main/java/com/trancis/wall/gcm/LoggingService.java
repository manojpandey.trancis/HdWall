package com.trancis.wall.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.trancis.wall.MyApplication;
import com.trancis.wall.R;
import com.trancis.wall.util.Preferences;

import java.io.IOException;

/**
 * Created by trancis on 6/9/17.
 */

public class LoggingService extends IntentService {
    public static final String LOG_TAG = "GCMLoggingService";

    public LoggingService()
    {
        super(LoggingService.class.getName());
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);
        String senderId = getResources().getString(R.string.gcm_app_id);
        try {
            // request token that will be used by the server to send push notifications
            String token = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
            Log.d(LOG_TAG, "GCM Registration Token: " + token);
            // pass along this data
            sendRegistrationToServer(token);
        } catch (IOException e) {
            e.printStackTrace();
            Preferences.setGCMTokenSent(MyApplication.getContext(), false);
            Preferences.setGCMToken(MyApplication.getContext(), "");
        }
    }

    private void sendRegistrationToServer(String token) {
        Preferences.setGCMTokenSent(MyApplication.getContext(), true);
        Preferences.setGCMToken(MyApplication.getContext(), token);

    }
}