package com.trancis.wall.util;

import com.trancis.wall.java_class.MediaModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by trancis on 25/8/17.
 */

public class Util {

    public static void sortListByLong(List<MediaModel> sList){
        Collections.sort(sList, new Comparator<MediaModel>() {
            public int compare(MediaModel o1, MediaModel o2) {
                return Long.valueOf(o2.modifiedDate).compareTo(Long.valueOf(o1.modifiedDate));
            }
        });


    }
}
