package com.trancis.wall.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by trancis on 6/9/17.
 */

public class Preferences {
    private static final String TAG = "HdWall::";
    public static final String SENT_TOKEN_TO_SERVER = TAG + "sentTokenToServer";
    public static final String GCMTOKEN = TAG + "GCMTOKEN";


    private static SharedPreferences getPreferences(Context ctx) {
        return ctx.getSharedPreferences(TAG, Context.MODE_PRIVATE);
    }

    public static void setGCMTokenSent(Context ctx, boolean value) {
        setValue(ctx, SENT_TOKEN_TO_SERVER, value);
    }

    public static void setGCMToken(Context ctx, String value) {
        setValue(ctx, GCMTOKEN, value);
    }

    private static void setValue(Context ctx, String key, boolean value) {
        SharedPreferences prefs = getPreferences(ctx);
        prefs.edit().putBoolean(key, value).apply();
    }

    private static void setValue(Context ctx, String key, String value) {
        SharedPreferences prefs = getPreferences(ctx);
        prefs.edit().putString(key, value).apply();
    }

}
