package com.trancis.wall.util;

import android.view.View;

/**
 * Created by trancis on 26/8/17.
 */

public interface ItemClickListener {
    void onItemClick(View view, int position);
    public void onItemLongClick(View view, int position);
}