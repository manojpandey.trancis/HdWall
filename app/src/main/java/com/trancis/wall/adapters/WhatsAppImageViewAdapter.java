package com.trancis.wall.adapters;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.trancis.wall.R;
import com.trancis.wall.java_class.MediaModel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by trancis on 26/8/17.
 */

public class WhatsAppImageViewAdapter extends PagerAdapter {

    Context mContext;
    List<MediaModel> mList = new ArrayList<>();
    LayoutInflater layoutInflater;
    int height = 400, width = 200;
   public Picasso picasso;

    public WhatsAppImageViewAdapter(Context mContext, List<MediaModel> list) {
        this.mContext = mContext;
        this.mList = list;
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        picasso = Picasso.with(mContext);
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.whatsapp_data_item, container, false);
        MediaModel mediaModel = mList.get(position);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageviewWhatsapp);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        picasso.load(new File(mediaModel.url)).resize(width,height).centerInside().placeholder(R.drawable.black)
                .into(imageView);
        container.addView(itemView);


        //setImage(imageView, progressBar, imageListAll.get(position),position);


        //Toast.makeText(this,"url is",Toast.LENGTH_LONG).show();


        return itemView;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //    container.removeView(images.get(position));
    }
}
