package com.trancis.wall.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trancis.wall.activity.DataInfoAll;
import com.trancis.wall.R;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by mohit on 05-07-2017.
 */
public class ImageAdapterAll extends ArrayAdapter {
    private Context mContext;
    private List<DataInfoAll> arrayList;
    LayoutInflater layoutInflater;
    boolean flag=true;
    // Constructor
    public ImageAdapterAll(Context c,int viewId, List<DataInfoAll> arrayList) {
        super(c, viewId, arrayList);
        mContext = c;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(mContext);

    }


    private void downloadImage(final DataInfoAll dataInfoAll, final ImageView imageView) {
        if (this.arrayList == null) {
            return;
        }


        dataInfoAll.isDownloading = true;

        new AsyncTask<Void, Bitmap, Bitmap>() {
            InputStream in = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //progressBar.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
                //imageView.setImageResource(R.drawable.galleryicon);
            }

            @Override
            protected void onPostExecute(Bitmap nBitmap) {
                super.onPostExecute(nBitmap);
                if (nBitmap != null) {
                    imageView.setImageBitmap(nBitmap);

                    dataInfoAll.bitmap = nBitmap;
                    notifyDataSetChanged();

                } else {
                    //imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.galleryicon));

                    dataInfoAll.isDownloading = false;

                }
                //imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.galleryicon));
                //progressBar.setVisibility(View.GONE);
                //imageView.setVisibility(View.VISIBLE);

                if(!flag) {
                    Toast.makeText(getContext(), "error  server not responding ",
                            Toast.LENGTH_SHORT).show();
                    imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.galleryicon));
                }
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    in = openHttpConnection(dataInfoAll.getThumbUrl());
                    if(in!=null)
                    {
                        Bitmap bitmap = BitmapFactory.decodeStream(in);
                        in.close();
                        return bitmap;
                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private InputStream openHttpConnection(String urlStr) {
        InputStream in = null;
        int resCode = -1;

        try {
            URL url = new URL(urlStr);
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.setConnectTimeout(3000);
            httpConn.setReadTimeout(6000);
            httpConn.connect();
            resCode = httpConn.getResponseCode();

            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            if(e instanceof TimeoutException)
            {
                flag=false;
            }
            //flag=false;
            //Log.e(LOG_TAG, "CONNECTION ERROR  FUNDAMO SERVER NOT RESPONDING", e);
            e.printStackTrace();
        }
        return in;
    }


    private class ViewHolder {
        ImageView imageView;
        //ProgressBar progressBar;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        //   ImageView imageView;
        // ProgressBar progressBar;

        DataInfoAll dataInfoAll = arrayList.get(position);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.gridviewitemall, null);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageview);
            //holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_bar);
            //holder.progressBar.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
            //  holder.progressBar = new ProgressBar(mContext);
            //   progressBar = new ProgressBar(mContext);
            // imageView = new ImageView(mContext);
            // holder.imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 260));
            //     holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.imageView.setPadding(1, 1, 1, 1);
            // convertView = holder.imageView;
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            // imageView = (ImageView) convertView;
        }

        if (dataInfoAll.bitmap != null) {
          //  holder.progressBar.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.VISIBLE);
            holder.imageView.setImageBitmap(dataInfoAll.bitmap);

        } else {
          //  holder.progressBar.setVisibility(View.VISIBLE);
            //holder.imageView.setVisibility(View.GONE);
            holder.imageView.setImageDrawable(ContextCompat.getDrawable(mContext,R.drawable.galleryicon));
          //  if (!dataInfoAll.isDownloading)
               downloadImage(dataInfoAll, holder.imageView);
        }
        return convertView;
    }

}
