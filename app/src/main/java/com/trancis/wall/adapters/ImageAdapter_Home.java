package com.trancis.wall.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;


import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trancis.wall.R;
import com.trancis.wall.java_class.DataInfoHome;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.TimeoutException;

///**
//* Created by Mohit Sharma on 09-06-2017.
//*/
public class ImageAdapter_Home extends ArrayAdapter {
    private Context mContext;
    private List<DataInfoHome> arrayList;
    ProgressDialog progress;
    LayoutInflater layoutInflater;
    boolean flag=true;

    // Constructor
    public ImageAdapter_Home(Context c, int viewId, List<DataInfoHome> arrayList) {
        super(c, viewId, arrayList);
        mContext = c;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(mContext);
    }

    private void downloadImage(final DataInfoHome dataInfoHome, final ImageView imageView, final ProgressBar progressBar) {
        if (this.arrayList == null) {
            return;
        }

        dataInfoHome.isDownloading = true;

        new AsyncTask<Void, Bitmap, Bitmap>() {
            InputStream in = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
               /* progress.setMessage("Loading");
                progress.show();
               */
                ///  imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.loader1));
                progressBar.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
            }

            @Override
            protected void onPostExecute(Bitmap nBitmap)
            {
                super.onPostExecute(nBitmap);

                if (nBitmap != null) {
                  //  imageView.setImageBitmap(nBitmap);

                    dataInfoHome.bitmap = nBitmap;
                    notifyDataSetChanged();
                }
                else
                {
                    imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.galleryicon));

                    dataInfoHome.isDownloading = false;

                }
                //imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.loader1));
                progressBar.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                if(!flag) {
                    Toast.makeText(getContext(), "error  server not responding ",
                            Toast.LENGTH_SHORT).show();
                    imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.galleryicon));
                }
                }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    in = openHttpConnection(dataInfoHome.getThumbUrl());
                    if(in!=null)
                    {
                        Bitmap bitmap = BitmapFactory.decodeStream(in);
                        in.close();
                        return bitmap;
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private InputStream openHttpConnection(String urlStr) {
        InputStream in = null;
        int resCode = -1;

        try {
            URL url = new URL(urlStr);
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            resCode = httpConn.getResponseCode();

            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            if(e instanceof TimeoutException)
            {
                flag=false;
            }
            //flag=false;
            e.printStackTrace();
        }
        return in;
    }

    private class ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        //   ImageView imageView;
        // ProgressBar progressBar;

        DataInfoHome dataInfoHome = arrayList.get(position);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.gridview_item, null);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageview);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_bar);
            holder.progressBar.getIndeterminateDrawable().setColorFilter(0xFF2c9bf4, android.graphics.PorterDuff.Mode.MULTIPLY);

            //  holder.progressBar = new ProgressBar(mContext);
            //   progressBar = new ProgressBar(mContext);
            // imageView = new ImageView(mContext);
            // holder.imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 260));
            //     holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.imageView.setPadding(1, 1, 1, 1);
            // convertView = holder.imageView;
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            // imageView = (ImageView) convertView;
        }

        if (dataInfoHome.bitmap != null) {
            holder.progressBar.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.VISIBLE);
            holder.imageView.setImageBitmap(dataInfoHome.bitmap);

        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.GONE);
            if (!dataInfoHome.isDownloading)
                downloadImage(dataInfoHome, holder.imageView, holder.progressBar);
        }
        return convertView;
    }
}
