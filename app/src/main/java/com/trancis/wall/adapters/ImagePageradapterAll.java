package com.trancis.wall.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.trancis.wall.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 05-07-2017.
 */
public class ImagePageradapterAll extends PagerAdapter {

    Context context;
    List<String> imageListAll = new ArrayList<>();

    LayoutInflater layoutInflater;
    Bitmap nBitmap;
    int height = 400, width = 200;
    HashMap<Integer, Bitmap> nBitmapHashMap;
    Picasso picasso;


    public ImagePageradapterAll(Context context, List<String> imageListAll) {
        this.context = context;
        this.imageListAll = imageListAll;
        nBitmapHashMap = new HashMap<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        picasso = Picasso.with(context);
    }




 /*   public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = images.get(position);
        container.addView(imageView);
        return imageView;
    }
*/

    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.imageitemforall, container, false);
        /*progress = new ProgressDialog(context);
        progress.setMessage("Logging In");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();
*/
 /*       ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.loader1);
*/

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageViewall);
        ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);

        container.addView(itemView);

        //setImage(imageView, progressBar, imageListAll.get(position),position);

        if (nBitmapHashMap.get(position) != null)
        {
            progressBar.setVisibility(View.GONE);
            imageView.setImageBitmap(nBitmapHashMap.get(position));
        }

        else
            setImage(imageView, progressBar, imageListAll.get(position), position);

        //Toast.makeText(this,"url is",Toast.LENGTH_LONG).show();


        return itemView;
    }

    private void setImage(final ImageView imageView, final ProgressBar progressBar, final String url, final int pos) {
        new AsyncTask<Void, Bitmap, Bitmap>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    return picasso.load(url).resize(width,height).get();
                } catch (Exception ex) {

                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                if (bitmap != null) {

                    picasso.invalidate(url);
                    imageView.setImageBitmap(bitmap);
                    progressBar.setVisibility(View.GONE);
                    //  nBitmap = bitmap;
                    nBitmapHashMap.put(pos, bitmap);
                } else {
                    imageView.setImageResource(R.drawable.galleryicon);
                }
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        //    container.removeView(images.get(position));
    }

    public int getCount() {
        return imageListAll.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    public Bitmap getBitmap(int pos) {
        return nBitmapHashMap.get(pos);

        // return nBitmap;
    }
}