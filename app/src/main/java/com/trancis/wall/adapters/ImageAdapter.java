package com.trancis.wall.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.trancis.wall.R;
import com.trancis.wall.java_class.DataInfoCat;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by mohit on 07-06-2017.
 */
public class ImageAdapter extends ArrayAdapter
{
    private Context mContext;
    private List<DataInfoCat> arrayList;
    LayoutInflater layoutInflater;
    boolean flag=true;
    // Constructor
    public ImageAdapter(Context c, int viewId, List<DataInfoCat> arrayList) {
        super(c, viewId, arrayList);
        mContext = c;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(mContext);
    }

    private void downloadCatImage(final DataInfoCat dataInfoCat, final ImageView imageView, final ProgressBar progressBar, final TextView textViewCategory) {
        if (this.arrayList == null) {
            return;
        }


        dataInfoCat.isDownloading = true;

        new AsyncTask<Void, Bitmap, Bitmap>() {
            InputStream in = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
                textViewCategory.setVisibility(View.GONE);
            }

            @Override
            protected void onPostExecute(Bitmap nBitmap) {
                super.onPostExecute(nBitmap);
                if (nBitmap != null) {
                    imageView.setImageBitmap(nBitmap);
                    dataInfoCat.bitmap = nBitmap;
                    textViewCategory.setText(dataInfoCat.getCategoryname()+" ("+dataInfoCat.getTotal()+")");
                } else {
                    imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.galleryicon));
                    textViewCategory.setText(R.string.categorynamestring);

                    dataInfoCat.isDownloading = false;
                }

                progressBar.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                textViewCategory.setVisibility(View.VISIBLE);
                if(!flag) {
                    Toast.makeText(getContext(), "error  server not responding ",
                            Toast.LENGTH_SHORT).show();
                    imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.galleryicon));
                    textViewCategory.setText(R.string.categorynamestring);
                }
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    in = openHttpConnection(dataInfoCat.getRealUrl());
                    if(in!=null) {
                        Bitmap bitmap = BitmapFactory.decodeStream(in);
                        in.close();
                        return bitmap;
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private InputStream openHttpConnection(String urlStr) {
        InputStream in = null;
        int resCode = -1;

        try {
            URL url = new URL(urlStr);
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.setConnectTimeout(5000);
            httpConn.setReadTimeout(10000);
            httpConn.connect();
            resCode = httpConn.getResponseCode();

            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            if(e instanceof TimeoutException)
            {
                flag  = false;
            }
            //flag  = false;
            e.printStackTrace();
        }
        return in;
    }

    private class ViewHolder
    {
        ImageView imageView;
        ProgressBar progressBar;
        TextView textViewCategory;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

//        ImageView imageView;
        DataInfoCat dataInfoCat = arrayList.get(position);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.gridviewitemcategory, null);
            holder.textViewCategory = (TextView) convertView.findViewById(R.id.categorytext);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageviewcategory);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress_barcategory);
            holder.progressBar.getIndeterminateDrawable().setColorFilter(0xFF2c9bf4, android.graphics.PorterDuff.Mode.MULTIPLY);
            //  holder.progressBar = new ProgressBar(mContext);
            //   progressBar = new ProgressBar(mContext);
            // imageView = new ImageView(mContext);
            // holder.imageView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 260));
            //     holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            holder.imageView.setPadding(1, 1, 1, 1);
            // convertView = holder.imageView;
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            // imageView = (ImageView) convertView;
        }

        if (dataInfoCat.bitmap != null) {
            holder.progressBar.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.VISIBLE);
            holder.textViewCategory.setVisibility(View.VISIBLE);
            holder.imageView.setImageBitmap(dataInfoCat.bitmap);
            holder.textViewCategory.setText(dataInfoCat.getCategoryname()+" ("+dataInfoCat.getTotal()+")");

        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.GONE);
            holder.textViewCategory.setVisibility(View.GONE);

            if (!dataInfoCat.isDownloading)
                downloadCatImage(dataInfoCat, holder.imageView, holder.progressBar,holder.textViewCategory);
        }
        return convertView;
    }
}
