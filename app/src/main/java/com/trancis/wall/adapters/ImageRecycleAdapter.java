package com.trancis.wall.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.squareup.picasso.Picasso;
import com.trancis.wall.R;
import com.trancis.wall.java_class.BitmapData;
import com.trancis.wall.java_class.MediaModel;
import com.trancis.wall.util.ItemClickListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * Created by trancis on 25/8/17.
 */


public class ImageRecycleAdapter extends RecyclerView.Adapter<ImageRecycleAdapter.ViewHolder> {
    List<MediaModel> mList;
    Context context;
    int width, height;
    ItemClickListener clickListener;
    int videoId = 10000101;
    boolean isVideo;
    private int mediaType;
    List<BitmapData> bitmapDataList = new ArrayList<BitmapData>();

    public ImageRecycleAdapter(List<MediaModel> mList, Context context) {
        this.context = context;
        this.mList = mList;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        width = metrics.widthPixels / 2;
        height = width + 20;

    }

    public void setMediaType(int type) {
        this.mediaType = type;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        ImageView imageView, imgIcon;
        LinearLayout linearVideo;

        public ViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.sdCardImageView);
            imgIcon = (ImageView) view.findViewById(R.id.imgVideoIcon);
            linearVideo = (LinearLayout) view.findViewById(R.id.linearVideo);
            if (mediaType == 0) {
                imgIcon.setVisibility(View.GONE);
                linearVideo.setVisibility(View.GONE);
            } else if (mediaType == 1) {
                imgIcon.setVisibility(View.VISIBLE);
                linearVideo.setVisibility(View.VISIBLE);
            } else {

            }
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) clickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            if (clickListener != null)
                clickListener.onItemLongClick(view, getAdapterPosition());
            return true;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sdcard_recyle_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MediaModel mediaModel = mList.get(position);
        try {
            // setPic(mediaModel, holder.imageView);
            // imageLoader.displayImage(mediaModel.url,holder.imageView);
            //holder.imageView.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.default_thumbnail));
            try {
                if (mediaType == 0)
                    Picasso.with(context).load(new File(mediaModel.url)).placeholder(R.drawable.default_thumbnail).resize(width, height).centerCrop().into(
                            holder.imageView);

                else if (mediaType == 1 || mediaType == 2) {
                    if (!mediaModel.isLoading) {
                        mediaModel.isLoading = true;
                        if (mediaModel.bitmap != null) {
                            holder.imageView.setImageBitmap(mediaModel.bitmap);
                            mediaModel.isLoading = false;
                        } else
                            setVideoImageThumbnail(mediaModel, holder.imageView);
                 /*       BitmapData bitmapData = filterBitmapListByUrl(mediaModel.url);
                        if (bitmapData != null) {
                            holder.imageView.setImageBitmap(bitmapData.bitmap);
                        } else
                            setVideoImageThumbnail(mediaModel, holder.imageView);*/
                    }

                  /*  Bitmap bmThumbnail = ThumbnailUtils.createVideoThumbnail(mediaModel.url,
                            MediaStore.Video.Thumbnails.MINI_KIND);
                    if (bmThumbnail != null)
                        holder.imageView.setImageBitmap(bmThumbnail);*/
                }
            } catch (Exception ex) {
                ex.printStackTrace();

            }
            //setPic(mediaModel, holder.imageView);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    private void setVideoImageThumbnail(final MediaModel model, final ImageView imageView) {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Bitmap doInBackground(Void... voids) {
                try {
                    return ThumbnailUtils.createVideoThumbnail(model.url,
                            MediaStore.Video.Thumbnails.MINI_KIND);

                } catch (Exception ex) {
                }

                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                if (bitmap != null) {
                  /*  BitmapData bitmapData = new BitmapData(model.url, bitmap);
                    if (bitmapDataList.size() >= 20) {
                        bitmapDataList.remove(0);
                        bitmapDataList.set(0,bitmapData);
                    }
                    else
                        bitmapDataList.add(bitmapData);*/
//                  imageView.setImageBitmap(bitmap);
                    model.bitmap = bitmap;

                }
                model.isLoading = false;
                notifyDataSetChanged();

            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }


    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }


    public BitmapData filterBitmapListByUrl(final String url) {
        List<BitmapData> appList = Lists.newArrayList(Collections2.filter(bitmapDataList, new com.google.common.base.Predicate<BitmapData>() {
            @Override
            public boolean apply(BitmapData obj) {
                return obj.url.equals(url);
            }
        }));
        if (appList.size() > 0)
            return appList.get(0);
        else
            return null;

    }
}
