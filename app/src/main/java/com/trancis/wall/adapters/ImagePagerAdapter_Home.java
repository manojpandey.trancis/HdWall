package com.trancis.wall.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.trancis.wall.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohit on 20-06-2017.
 */
public class ImagePagerAdapter_Home extends PagerAdapter {
    Context context;
    List<String> imageListHome = new ArrayList<>();
    public List<ImageView> images;
    LayoutInflater layoutInflater;
    HashMap<Integer, Bitmap> mBitmapHashMap ;


    public ImagePagerAdapter_Home(Context context, List<String> imageListHome) {
        this.context = context;
        this.imageListHome = imageListHome;
        mBitmapHashMap = new HashMap<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
 /*   public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = images.get(position);
        container.addView(imageView);
        return imageView;
    }
*/

    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.imageitempagerhome, container, false);
/*
        progresss = new ProgressDialog(context);
        progresss.setMessage("loading");
        progresss.setCancelable(false);
*/



 /*       ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.loader1);
*/
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
/*
        progresss.show();
*/


        container.addView(itemView);

        // Glide.with(context).load(imageListHome.get(position)).into(imageView);
        if(mBitmapHashMap.get(position)!=null)
            imageView.setImageBitmap(mBitmapHashMap.get(position));
        else
        setImage(imageView,progressBar,position);

        //listening to image click
/*
        progresss.dismiss();
*/


        // Intent intentbit = new Intent(context, Zoom_for_Home.class);
        //intentbit.putExtra("key", bitmap);

        return itemView;
    }

    private void setImage(final ImageView imageView,final ProgressBar progressBar, final int position) {
        final String url = imageListHome.get(position);
        new AsyncTask<Void, Bitmap, Bitmap>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Bitmap doInBackground(Void... params) {
                try
                {
                    return Picasso.with(context).load(url).get();
                }
                catch (Exception ex)
                {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap)
            {
                super.onPostExecute(bitmap);
                if (bitmap != null)

                    imageView.setImageBitmap(bitmap);
                    progressBar.setVisibility(View.GONE);
                if(bitmap != null)
                {
                    mBitmapHashMap.put(position,bitmap);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //    container.removeView(images.get(position));
    }

    @Override
    public int getCount()
    {
        return imageListHome.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    public Bitmap getBitmap(int position) {
        return mBitmapHashMap.get(position);
    }
}