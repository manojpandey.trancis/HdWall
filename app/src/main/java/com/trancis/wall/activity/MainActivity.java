
/*
       -->   facebook data storage problem
       -->   rating bar ui
       -->   facebook logout button
*/


package com.trancis.wall.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.trancis.wall.R;
import com.trancis.wall.fragments.Categories;
import com.trancis.wall.fragments.Fragment_Rating;
import com.trancis.wall.fragments.Gifs;
import com.trancis.wall.fragments.MainFragment;
import com.trancis.wall.fragments.Updated_Profile;
import com.trancis.wall.fragments.WhatsAppGallary;
import com.trancis.wall.gcm.LoggingService;
import com.trancis.wall.java_class.SessionManager;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {
    ImageView imageView;

    private GoogleApiClient googleApiClient;
    SessionManager session;
    android.support.v4.app.FragmentManager fm;
    DrawerLayout drawer;
    private List<Integer> itemPositionStacks = new ArrayList<>();
    NavigationView navigationView;
    int selectedId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getSupportFragmentManager();
        setContentView(R.layout.activity_main);
         String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        imageView = (ImageView) findViewById(R.id.imageViewUpdated);


        // Session class instance
        session = new SessionManager(getApplicationContext());

        isOnline();
        isStoragePermissionGranted();

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions
                .DEFAULT_SIGN_IN).requestEmail().build();
        try {

            googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }


        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
        //SessionManager.pref.edit().putBoolean("IsLoggedIn", false).commit();
        if (!session.checkLogin()) {
            finish();
        } else {
            // start gcm service
           // startService(new Intent(this, LoggingService.class));
            fm.beginTransaction().replace(R.id.relative_layout, new MainFragment()).commit();

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    //   getActionBar().setTitle(mTitle);
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    //  getActionBar().setTitle(mDrawerTitle);
                }
            };


            drawer.setDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            //   View view= navigationView.findViewById(R.id.imageViewUpdated);
            navigationView.setCheckedItem(R.id.nav_latest);
            selectedId = R.id.nav_latest;

            updateDrawerImage(navigationView);
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
                return true;
            } else {
                //finish();
                // Toast.makeText(MainActivity.this, "Permission revoked", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            //Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(MainActivity.this,"Permission: "+permissions[0]+ "was "+grantResults[0], Toast.LENGTH_SHORT).show();
            //resume tasks needing this permission
        } else {

        }
    }

    public void updateDrawerImage(NavigationView navigationView) {

        CircleImageView imageView = null;
        View headerLayout = navigationView.getHeaderView(0);
        if (headerLayout != null) {
            imageView = (CircleImageView) headerLayout.findViewById(R.id.imageViewUpdated);

        }
        String uri = session.getProfileImage();
        if (uri != null) {
            //Toast.makeText(this, uri, Toast.LENGTH_LONG).show();
            if (imageView != null) {
                imageView.setImageURI(Uri.parse(uri));
            }
        }
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("OOPS");
            builder.setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    finish();
                }
            })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {

        // DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Exit Confirmation");
                builder.setMessage("Are you sure you want to Exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                nbutton.setTextColor(Color.BLACK);
                Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                pbutton.setTextColor(Color.BLACK);

            /*if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStackImmediate();
            } else {
                super.onBackPressed();
            }*/
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                // After logout redirect user to Loing Activity
                session.logoutUser();
            }
        });
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_categories) {
            selectedId = R.id.nav_categories;
            Categories cat = new Categories();
            FragmentTransaction transaction = fm.beginTransaction().replace(R.id.relative_layout, cat);
            transaction.addToBackStack(null);
            transaction.commit();
           /* fm.beginTransaction().replace(R.id.relative_layout,cat).commit();
            Toast.makeText(this,"Categories",Toast.LENGTH_LONG).show();*/
        }
        else if (id == R.id.nav_latest) {
            selectedId = R.id.nav_latest;
            MainFragment mf = new MainFragment();
            FragmentTransaction transaction = fm.beginTransaction().replace(R.id.relative_layout, mf);
            transaction.addToBackStack(null);
            transaction.commit();
           /* fm.beginTransaction().replace(R.id.relative_layout,cat).commit();
            Toast.makeText(this,"Categories",Toast.LENGTH_LONG).show();*/
        }
        else if (id == R.id.nav_gif) {
            selectedId = R.id.nav_gif;
            Gifs gifs = new Gifs();
            FragmentTransaction transaction = fm.beginTransaction().replace(R.id.relative_layout, gifs);
            transaction.addToBackStack(null);
            transaction.commit();
           /* fm.beginTransaction().replace(R.id.relative_layout,cat).commit();
            Toast.makeText(this,"Categories",Toast.LENGTH_LONG).show();*/
        } else if (id == R.id.nav_rating) {
            selectedId = R.id.nav_rating;
            Fragment_Rating rat = new Fragment_Rating();
            FragmentTransaction transaction = fm.beginTransaction().replace(R.id.relative_layout, rat);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.nav_logut) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Logout Confirmation");
            builder.setMessage("Are you sure you want to Logout?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            signOut();
                            session.logoutUser();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();

            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);


            if (FacebookSdk.isInitialized()) {
                LoginManager.getInstance().logOut();
            }
            //signOut();
            //session.logoutUser();
        } else if (id == R.id.nav_profile) {
            selectedId = R.id.nav_profile;
            Updated_Profile updated_profile = new Updated_Profile();

            FragmentTransaction transaction = fm.beginTransaction().replace(R.id.relative_layout, updated_profile);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        else if(id==R.id.nav_whatsapp){
            WhatsAppGallary whatsAppGallary = new WhatsAppGallary();
            FragmentTransaction transaction = fm.beginTransaction().replace(R.id.relative_layout, whatsAppGallary);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        else if (id == R.id.nav_terms_con) {
            item.setChecked(false);
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            builder.setTitle("TERMS AND CONDITIONS");

            builder.setMessage("This Site and all its Contents are intended solely for personal, non-commercial use. Except as expressly provided, nothing within the Site shall be construed as conferring any license under our or any third party's intellectual property rights, whether by estoppel, implication, waiver, or otherwise. Without limiting the generality of the foregoing, you acknowledge and agree that all content available through and used to operate the Site and its services is protected by copyright, trademark, patent, or other proprietary rights. You agree not to: (a) modify, alter, or deface any of the trademarks, service marks, trade dress (collectively \"Trademarks\") or other intellectual property made available by us in connection with the Site; (b) hold yourself out as in any way sponsored by, affiliated with, or endorsed by us, or any of our affiliates or service providers; (c) use any of the Trademarks or other content accessible through the Site for any purpose other than the purpose for which we have made it available to you; (d) defame or disparage us, our Trademarks, or any aspect of the Site; and (e) adapt, translate, modify, decompile, disassemble, or reverse engineer the Site or any software or programs used in connection with it " +
                    "or its products and services.")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            navigationView.setCheckedItem(selectedId);
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

        } else if (id == R.id.nav_about_us) {

            item.setChecked(false);
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            builder.setTitle("ABOUT US");

            builder.setMessage("Tranciscolabs, a main Android App Development Company in Gurgaon that utilizes innovative ideas, polished methodology and involvement in Android application programming, Android application testing, Android application planning to improve the best ease of use, simple use and client encounters. We guarantee a significantly responsive and mindful setting for Android applications by utilizing upgraded extend programming and novel correspondence advances.\n" +
                    "\n")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            navigationView.setCheckedItem(selectedId);
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

        } else if (id == R.id.nav_privacypolicy) {

            item.setChecked(false);
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            builder.setTitle("PRIVACY POLICY");

            builder.setMessage("Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n" + "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n" + "Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\n" + "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry.\n" + "Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.\n" + "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of " +
                    "Lorem Ipsum.").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();

                            navigationView.setCheckedItem(selectedId);
                        }
                    });


            AlertDialog alert = builder.create();

            //Display the message!
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

            //Privacy_Dialog privacy_dialog=new Privacy_Dialog();
            //android.support.v4.app.FragmentManager fm3=getSupportFragmentManager();
            //fm3.beginTransaction().replace(R.id.relative_layout,privacy_dialog).commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setDrawerImage(Intent data) {
        Uri selected_image = data.getData();
        ((ImageView) findViewById(R.id.imageViewUpdated)).setImageURI(selected_image);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        int a=10;
        int b=11;
        b=a;
        a=b;

    }
}
