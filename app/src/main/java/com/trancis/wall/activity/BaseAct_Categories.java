package com.trancis.wall.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.trancis.wall.R;
import com.trancis.wall.adapters.ImageAdapterAll;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BaseAct_Categories extends AppCompatActivity
{

    JSONObject jsonobject;
    JSONArray jsonarray;
    GridView gridView2;
    int no;
    String title;
    ProgressBar progressBar;
    String urlstr =  "http://www.viaviweb.in/envato/cc/hd_wallpaper_demo/api.php?cat_id=";
    Handler handler;
    ImageAdapterAll adapter;
    int a = 1;
    List<DataInfoAll> dataListAll = new ArrayList<>();
    List<String> urlListAll=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_act__categories);
        no = getIntent().getIntExtra("cid", 0);
        title = getIntent().getStringExtra("category_name");

        urlstr = urlstr.concat(String.valueOf(no));

        isOnline();

        handler = new Handler();

        this.setTitle(title);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar_baseact_category);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFF2c9bf4, android.graphics.PorterDuff.Mode.MULTIPLY);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gridView2 = (GridView) findViewById(R.id.gridView2);
        /*gridView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"Call",Toast.LENGTH_SHORT).show();
            }
        });*/

/*
gridView2
        gridView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id)
            {
                Toast.makeText(BaseAct_Categories.this, "" + position,
                        Toast.LENGTH_SHORT).show();
            }
        });
*/

        gridView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DataInfoAll dataInfoAll = dataListAll.get(position);
                Intent intentall = new Intent(BaseAct_Categories.this, Zoomforall.class);
                intentall.putExtra("urls",(Serializable)urlListAll);
                intentall.putExtra("posit",position);
                intentall.putExtra("title1",title);
                startActivity(intentall);
            }
        });

        Runnable runnable = new Runnable() {
            public void run() {
                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(urlstr)
                        .build();
                Call call;

                call = client.newCall(request);
                if (call != null) {
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                                  //isOnline();
                            ConnectivityManager conMgr = (ConnectivityManager) BaseAct_Categories.this.getSystemService(BaseAct_Categories.this.CONNECTIVITY_SERVICE);
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

                            if (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()) {

                                BaseAct_Categories.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(BaseAct_Categories.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                       }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            parseResponse(response);
                        }
                    });
                }
            }
        };
        runnable.run();

    }


    private void parseResponse(Response response) {
        String jsonData = null;


        try {
            jsonData = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            jsonobject = new JSONObject(jsonData);
            jsonarray = jsonobject.getJSONArray("HD_WALLPAPER");
            for (int i = 0; i < jsonarray.length(); i++) {
                jsonobject = jsonarray.getJSONObject(i);
                DataInfoAll dataInfoAll = new DataInfoAll(jsonobject.getInt("id"),jsonobject.getInt("cat_id"), jsonobject.getString("wallpaper_image"), jsonobject.getString("wallpaper_image_thumb"));
                dataListAll.add(dataInfoAll);
//                Toast.makeText(this,"  ss  ",Toast.LENGTH_LONG).show();
            }

            for (DataInfoAll infoAll : dataListAll) {
                urlListAll.add(infoAll.getRealUrl());
            }

            updateAdapter();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean onSupportNavigateUp()
    {
        this.finish();
        return true;
    }
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("OOPS");
            builder.setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

            return false;
        }
        return true;
    }
    private void updateAdapter() {

        //    adapter = new ImageAdapter_Home(getActivity(), dataListHome);
        // Set the adapter to the ListView
        //  gridView.setAdapter(adapter);
        final  Runnable runnable = new Runnable()
        {
            public void run() {
                // Pass the results into ListViewAdapter.java
                adapter = new ImageAdapterAll(BaseAct_Categories.this,R.layout.gridviewitemall, dataListAll);
                // Set the adapter to the ListView
                progressBar.setVisibility(View.GONE);
                gridView2.setAdapter(adapter);

            }
        };
        //runnable.run();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        });
    }
}
