package com.trancis.wall.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.trancis.wall.R;

public class Recovery_done extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery_done);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Password Recovery");
    }
    public boolean onSupportNavigateUp()
    {
        this.finish();
        return true;
    }
}
