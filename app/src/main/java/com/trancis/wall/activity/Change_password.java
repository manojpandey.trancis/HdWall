package com.trancis.wall.activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.trancis.wall.R;
import com.trancis.wall.java_class.DataBaseHelper;
import com.trancis.wall.java_class.SessionManager;

public class Change_password extends AppCompatActivity implements View.OnClickListener {
    SessionManager session;
    EditText t4, t5, t6;
    String pass;
    String abc;
    Button b_pass;
    String old_password;
    String password;
    String new_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        t4 = (EditText) findViewById(R.id.editText11);
        t5 = (EditText) findViewById(R.id.editText12);
        t6 = (EditText) findViewById(R.id.editText13);
        b_pass = (Button) findViewById(R.id.button_pass);
        b_pass.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Session class instance
        session = new SessionManager(getApplicationContext());
        // get user data from session
        String key = session.getUserDetails();
        abc = key;
        DataBaseHelper dh = new DataBaseHelper(getApplicationContext());
        SQLiteDatabase db = dh.getReadableDatabase();

        String s = "select * from Login where emailid=?";
        Cursor c = db.rawQuery(s, new String[]{key});

        if (c.moveToFirst())
        {
            pass = c.getString(3);
        }
    }

    public boolean onSupportNavigateUp()
    {
        this.finish();
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_pass) {

            DataBaseHelper dh1 = new DataBaseHelper(getApplicationContext());
            SQLiteDatabase db1 = dh1.getWritableDatabase();

             old_password = t4.getText().toString();
             password = t5.getText().toString();
             new_password = t6.getText().toString();


            if (old_password.isEmpty() || new_password.isEmpty() || password.isEmpty())
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle("OOPS");

                builder.setMessage("Please fill all Feilds")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                ;
                AlertDialog alert = builder.create();
                alert.show();
                Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                nbutton.setTextColor(Color.BLACK);
                Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                pbutton.setTextColor(Color.BLACK);

            }
            else
            {
                if (pass.equals(old_password) && password.equals(new_password))
                {
                    ContentValues cv = new ContentValues();
                    cv.put("password", password);

                    int ans = db1.update("Login", cv, "emailid=?", new String[]{abc});

                    if (ans == 1)
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);

                        builder.setTitle("SUCCESS");

                        builder.setMessage("Password Updated")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        /*Intent sendback = new Intent(Change_password.this, MainActivity.class);
                                        startActivity(sendback);*/
                                        finish();
                                    }
                                })
                        ;
                        AlertDialog alert = builder.create();
                        alert.show();
                        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                        nbutton.setTextColor(Color.BLACK);
                        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                        pbutton.setTextColor(Color.BLACK);

                    }
                    else
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);

                        builder.setTitle("OOPS");

                        builder.setMessage("Incorrect Credentials")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                })
                        ;
                        AlertDialog alert = builder.create();
                        alert.show();
                        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                        nbutton.setTextColor(Color.BLACK);
                        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                        pbutton.setTextColor(Color.BLACK);

                    }
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle("OOPS");

                    builder.setMessage("Incorrect Credentials")
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                    ;
                    AlertDialog alert = builder.create();
                    alert.show();
                    Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                    nbutton.setTextColor(Color.BLACK);
                    Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    pbutton.setTextColor(Color.BLACK);

                }
            }

        }
    }
}