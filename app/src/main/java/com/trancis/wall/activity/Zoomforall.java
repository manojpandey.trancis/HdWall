package com.trancis.wall.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import com.trancis.wall.R;
import com.github.clans.fab.FloatingActionMenu;
import com.trancis.wall.adapters.ImagePageradapterAll;
import com.trancis.wall.java_class.DataInfoCat;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class Zoomforall extends AppCompatActivity implements View.OnClickListener {
    ViewPager viewPager;
    ProgressDialog progress1,progress2;
    //String myurl;
    //ImageView img;
    List<String> imageListAll = new ArrayList<>();
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton save_a, share_a, wall_a;
    DataInfoCat dataInfoCat;
    ImagePageradapterAll imagePageradapterAll;
    final int REQUEST_SAVE = 20001;
    final int REQUEST_SHARE = 20002;
    final int REQUEST_WALL = 20003;

    int position;
    String title;

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_zoomforall);


        save_a = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.imageButtonsaveall);
        save_a.setOnClickListener(this);

        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);

        wall_a = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.imageButtonsetwallpaper);
        wall_a.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        share_a = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.imageButtonshareall);
        share_a.setOnClickListener(this);



            Drawable myFabSrc = getResources().getDrawable(android.R.drawable.ic_menu_save);
//copy it in a new one
            Drawable willBeWhite = myFabSrc.getConstantState().newDrawable();
//set the color filter, you can use also Mode.SRC_ATOP
            willBeWhite.mutate().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
//set it to your fab button initialized before
            save_a.setImageDrawable(willBeWhite);


            try {
            title = getIntent().getStringExtra("title1");
            position = getIntent().getIntExtra("posit", 0);
            imageListAll = (ArrayList<String>) getIntent().getSerializableExtra("urls");
        } catch (Exception ex) {
        }

        if (isOnline()) {

            progress1 = new ProgressDialog(getWindow().getContext());
            progress1.setCancelable(false);

            progress2 = new ProgressDialog(getWindow().getContext());
            progress2.setCancelable(false);

            //progress2.setCancelable(false);


            this.setTitle(title);

            viewPager = (ViewPager) findViewById(R.id.view_pagerall);
            viewPager.setOffscreenPageLimit(0);
            imagePageradapterAll = new ImagePageradapterAll(Zoomforall.this, imageListAll);
            viewPager.setAdapter(imagePageradapterAll);
            viewPager.setCurrentItem(position);
        }

        //  myurl = getIntent().getStringExtra("url");
        //img = (ImageView) findViewById(R.id.imageView3);
//        img.setImageResource(R.drawable.loader1);

        //Picasso.with(this).load(myurl).into(img);

        /*/*ImageAdapter_Home imageAdapter_home = new ImageAdapter_Home(getApplicationContext());
        List<ImageView> images = new ArrayList<>();
*/
/*
            for (int i = 0; i < imageAdapter_home.getCount(); i++) {
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(imageAdapter_home.mThumbIds[i]);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            images.add(imageView);
        }
*/

        // Finally create the adapter
/*
        ImagePagerAdapter_Home imagePagerAdapter_base = new ImagePagerAdapter_Home(images);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(imagePagerAdapter_base);

        // Set the ViewPager to point to the selected image from the previous activity
        // Selected image id
        int position = getIntent().getExtras().getInt("id");
        viewPager.setCurrentItem(position);
*/
    }

    public boolean onSupportNavigateUp() {
        this.finish();
        return true;
    }


    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("OOPS");
            builder.setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();

            return false;
        }
        return true;
    }

    private void shareImage() {

        Bitmap bitmap = imagePageradapterAll.getBitmap(viewPager.getCurrentItem());
        String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "title", null);

        if (pathofBmp != null) {
            Uri bmpUri = Uri.parse(pathofBmp);
            final Intent shareIntent1 = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent1.setType("image/png");
            Intent chooser = Intent.createChooser(shareIntent1, "Choose a Medium to share :");
            startActivity(chooser);
        }
    }

    public void onClick(View v) {


        if (v.getId() == R.id.imageButtonshareall) {

            ConnectivityManager conMgr = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()) {

                if (iseStoragePermissionGranted(REQUEST_SHARE)) {
                    shareImage();
                }
            } else {
                Toast.makeText(Zoomforall.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        } else if (v.getId() == R.id.imageButtonsaveall) {

            ConnectivityManager conMgr = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()) {

                if (iseStoragePermissionGranted(REQUEST_SAVE)) {
                    try {
                        saveFile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Toast.makeText(Zoomforall.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        } else if (v.getId() == R.id.imageButtonsetwallpaper) {
            ConnectivityManager conMgr = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()) {

                if (iseStoragePermissionGranted(REQUEST_WALL)) {
                    try {
                        setwallpaper();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Toast.makeText(Zoomforall.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setwallpaper()
    {

       final Bitmap bitmap = imagePageradapterAll.getBitmap(viewPager.getCurrentItem());

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress2.setMessage("Just wait a second");
                progress2.show();

            }

            @Override
            protected Void doInBackground(Void... params)
            {
                Bitmap bitmapWallPaper;

                if (bitmap != null) {
                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(Zoomforall.this);
                    wallpaperManager.setWallpaperOffsetSteps(1, 1);

                    try {
                        DisplayMetrics displayMetrics = getResources().getSystem().getDisplayMetrics();
                        if (displayMetrics != null) {
                            int width = displayMetrics.widthPixels;
                            int height = displayMetrics.heightPixels;
                            wallpaperManager.suggestDesiredDimensions(width, height);
                            bitmapWallPaper = Bitmap.createScaledBitmap(bitmap, width, height, true);

//                    Toast.makeText(Zoomforall.this, "w ="+width+" h = "+height, Toast.LENGTH_SHORT).show();
                            //Bitmap myBitmap = Bitmap.createBitmap(width, height, bitmap.getConfig());


                        } else {
                            bitmapWallPaper = Bitmap.createBitmap(bitmap);
                        }

                        wallpaperManager.setBitmap(bitmapWallPaper);

                        //bitmap.recycle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progress2.cancel();
                Toast.makeText(Zoomforall.this, "Wallpaper Set", Toast.LENGTH_SHORT).show();
            }
        }.execute();
           /* progress2.setMessage("Just wait a second");
            progress2.show();

            Bitmap bitmap = imagePageradapterAll.getBitmap(viewPager.getCurrentItem());
            Bitmap bitmapWallPaper;

            if (bitmap != null) {
                WallpaperManager  wallpaperManager= WallpaperManager.getInstance(Zoomforall.this);
                wallpaperManager.setWallpaperOffsetSteps(1, 1);

                try {
                    DisplayMetrics displayMetrics = getResources().getSystem().getDisplayMetrics();
                    if (displayMetrics != null) {
                        int width = displayMetrics.widthPixels;
                        int height = displayMetrics.heightPixels;
                        wallpaperManager.suggestDesiredDimensions(width, height);
                        bitmapWallPaper = Bitmap.createScaledBitmap(bitmap,width,height,true);

//                    Toast.makeText(Zoomforall.this, "w ="+width+" h = "+height, Toast.LENGTH_SHORT).show();
                        //Bitmap myBitmap = Bitmap.createBitmap(width, height, bitmap.getConfig());



                    } else {
                        bitmapWallPaper = Bitmap.createBitmap(bitmap);
                    }

                    wallpaperManager.setBitmap(bitmapWallPaper);
                    progress2.cancel();
                    Toast.makeText(Zoomforall.this, "Wallpaper Set", Toast.LENGTH_SHORT).show();

                    //bitmap.recycle();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
*/

    }

   /* private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
    }
*/
    private void saveFile() {

        final Bitmap bitmap = imagePageradapterAll.getBitmap(viewPager.getCurrentItem());
        if (bitmap != null) {

            new AsyncTask<Void, String, String>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progress1.setMessage("Saving");
                    progress1.show();
                }

                @Override
                protected String doInBackground(Void... params) {
                    try {

                    } catch (Exception ex) {

                    }
                    String path = Environment.getExternalStorageDirectory().toString();
                    File filename = new File(path + "/HDWallPaper/" + System.currentTimeMillis() + ".jpg");
                    try {
                        new File(path + "/HDWallPaper").mkdirs();

                        FileOutputStream out = new FileOutputStream(filename);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        out.flush();
                        out.close();

                        MediaStore.Images.Media.insertImage(getContentResolver(), filename.getAbsolutePath(), filename.getName(), filename.getName());

                    } catch (Exception e) {
                    }


                    return filename.toString();
                }

                @Override
                protected void onPostExecute(String filename) {
                    super.onPostExecute(filename);
                    progress1.cancel();
                    Toast.makeText(getApplicationContext(), "File is Saved in  " + filename, Toast.LENGTH_SHORT).show();
                }
            }.execute();
        }
    }

    public boolean iseStoragePermissionGranted(int code) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
                return true;
            } else {
                //finish();
                // Toast.makeText(MainActivity.this, "Permission revoked", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, code);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            //Toast.makeText(MainActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == REQUEST_SAVE) {
                try {
                    saveFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_SHARE) {
                shareImage();
            } else if (requestCode == REQUEST_WALL) {
                setwallpaper();
            }
        }
       /* if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Bitmap bitmap = imagePageradapterAll.getBitmap(viewPager.getCurrentItem());
            String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "title", null);

            Uri bmpUri = Uri.parse(pathofBmp);
            final Intent shareIntent1 = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent1.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent1.setType("image/png");
            Intent chooser = Intent.createChooser(shareIntent1, "Choose a Medium to share :");
            startActivity(chooser);

            //Toast.makeText(MainActivity.this,"Permission: "+permissions[0]+ "was "+grantResults[0], Toast.LENGTH_SHORT).show();
            //resume tasks needing this permission
        } else {

        }*/
    }
}
