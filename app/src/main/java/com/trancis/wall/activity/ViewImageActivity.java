package com.trancis.wall.activity;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.trancis.wall.R;
import com.trancis.wall.adapters.ImagePageradapterAll;
import com.trancis.wall.adapters.WhatsAppImageViewAdapter;
import com.trancis.wall.java_class.DataInfoCat;
import com.trancis.wall.java_class.MediaModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by trancis on 26/8/17.
 */

public class ViewImageActivity extends AppCompatActivity implements View.OnClickListener {

    ViewPager viewPager;
    List<MediaModel> modelList = new ArrayList<>();
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton btnDelete, btnShare, btnSetWall;
    DataInfoCat dataInfoCat;
    WhatsAppImageViewAdapter viewAdapter;
    final int REQUEST_SAVE = 20001;
    final int REQUEST_SHARE = 20002;
    final int REQUEST_WALL = 20003;
    int width, height;
    DisplayMetrics displayMetrics;
    LinearLayout linearProgress;
    boolean isProgessShowing;

    int position;
    String title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoomforall);
        displayMetrics = getResources().getDisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark));
        }
        width = displayMetrics.widthPixels;
        height = displayMetrics.heightPixels;
        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);

        btnDelete = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.imageButtonsaveall);
        btnSetWall = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.imageButtonsetwallpaper);
        btnShare = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.imageButtonshareall);
        linearProgress = (LinearLayout) findViewById(R.id.linearProgress);
        viewPager = (ViewPager) findViewById(R.id.view_pagerall);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnDelete.setLabelText("Delete");
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), android.R.drawable.ic_menu_delete);
        btnDelete.setOnClickListener(this);
        btnSetWall.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnDelete.setImageDrawable(drawable);

        try {

            title = getIntent().getStringExtra("title");
            position = getIntent().getIntExtra("position", 0);
            modelList = (List<MediaModel>) getIntent().getSerializableExtra("lists");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        getSupportActionBar().setTitle(title);

        viewPager.setOffscreenPageLimit(0);
        viewAdapter = new WhatsAppImageViewAdapter(ViewImageActivity.this, modelList);
        viewPager.setAdapter(viewAdapter);
        viewPager.setCurrentItem(position);


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        int position = viewPager.getCurrentItem();
        MediaModel mediaModel = modelList.get(position);

        switch (id) {
            case R.id.imageButtonsaveall:

                break;
            case R.id.imageButtonsetwallpaper:
                setwallpaper(mediaModel.url);
                break;
            case R.id.imageButtonshareall:
                shareImage(mediaModel.url);
                break;
            default:

                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (isProgessShowing)
            return;
        else
            super.onBackPressed();
    }

    private void shareImage(String path) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("image/*");
        File fileToShare = new File(path);
        Uri uri = Uri.fromFile(fileToShare);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(sharingIntent, "Share Image "));

    }

    private void setwallpaper(final String path) {

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                isProgessShowing = true;
                linearProgress.setVisibility(View.VISIBLE
                );
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                Bitmap bitmapWallPaper;
                try {
                    final Bitmap bitmap = viewAdapter.picasso.load(new File(path)).resize(width, height).get();
                    if (bitmap != null) {
                        WallpaperManager wallpaperManager = WallpaperManager.getInstance(ViewImageActivity.this);
                        wallpaperManager.setWallpaperOffsetSteps(1, 1);


                        DisplayMetrics displayMetrics = getResources().getSystem().getDisplayMetrics();
                        if (displayMetrics != null) {
                            int width = displayMetrics.widthPixels;
                            int height = displayMetrics.heightPixels;
                            wallpaperManager.suggestDesiredDimensions(width, height);
                            bitmapWallPaper = Bitmap.createScaledBitmap(bitmap, width, height, true);

                        } else {
                            bitmapWallPaper = Bitmap.createBitmap(bitmap);
                        }

                        wallpaperManager.setBitmap(bitmapWallPaper);

                        //bitmap.recycle();
                        return true;
                    } else
                        return false;


                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }


            }

            @Override
            protected void onPostExecute(Boolean bool) {
                super.onPostExecute(bool);
                linearProgress.setVisibility(View.GONE);
                isProgessShowing = false;
                Toast.makeText(ViewImageActivity.this, bool ? "Wallpaper set successfully " : "Error to set wallpaper", Toast.LENGTH_SHORT).show();
            }
        }.execute();
           /* progress2.setMessage("Just wait a second");
            progress2.show();

            Bitmap bitmap = imagePageradapterAll.getBitmap(viewPager.getCurrentItem());
            Bitmap bitmapWallPaper;

            if (bitmap != null) {
                WallpaperManager  wallpaperManager= WallpaperManager.getInstance(Zoomforall.this);
                wallpaperManager.setWallpaperOffsetSteps(1, 1);

                try {
                    DisplayMetrics displayMetrics = getResources().getSystem().getDisplayMetrics();
                    if (displayMetrics != null) {
                        int width = displayMetrics.widthPixels;
                        int height = displayMetrics.heightPixels;
                        wallpaperManager.suggestDesiredDimensions(width, height);
                        bitmapWallPaper = Bitmap.createScaledBitmap(bitmap,width,height,true);

//                    Toast.makeText(Zoomforall.this, "w ="+width+" h = "+height, Toast.LENGTH_SHORT).show();
                        //Bitmap myBitmap = Bitmap.createBitmap(width, height, bitmap.getConfig());



                    } else {
                        bitmapWallPaper = Bitmap.createBitmap(bitmap);
                    }

                    wallpaperManager.setBitmap(bitmapWallPaper);
                    progress2.cancel();
                    Toast.makeText(Zoomforall.this, "Wallpaper Set", Toast.LENGTH_SHORT).show();

                    //bitmap.recycle();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
*/

    }
}
