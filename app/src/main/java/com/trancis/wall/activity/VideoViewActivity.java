package com.trancis.wall.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.trancis.wall.R;

import java.io.File;

/**
 * Created by trancis on 30/8/17.
 */

public class VideoViewActivity extends AppCompatActivity {
    VideoView videoView;
    ProgressBar progressBar;
    String path, title;
    MediaController mediaController;
    int mediaType = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoview);
        progressBar = new ProgressBar(this);
        videoView = (VideoView) findViewById(R.id.videoView);
        try {
            path = getIntent().getStringExtra("path");
            title = getIntent().getStringExtra("title");
            mediaType = getIntent().getIntExtra("mediaType", 1);
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            path = null;
        }

        if (TextUtils.isEmpty(path)) {
            finish();
        } else {
            mediaController = new MediaController(this);
            mediaController.setAnchorView(videoView);
            videoView.setMediaController(mediaController);
            Uri uri = Uri.parse(path);
            videoView.setVideoURI(uri);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    videoView.start();
                }
            }, 700);

        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.share_menu, menu);
        Drawable drawable = menu.findItem(R.id.action_share).getIcon();

        /*drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this,android.R.color.white));
        menu.findItem(R.id.action_share).setIcon(drawable);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            if (mediaType == 1)
                sharingIntent.setType("video/mp4");
            else
            sharingIntent.setType("image/gif");
            File fileToShare = new File(path);
            Uri uri = Uri.fromFile(fileToShare);
            sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(sharingIntent, mediaType != 2 ? "Share Video " : "Share Gif"));

        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);

    }
}
