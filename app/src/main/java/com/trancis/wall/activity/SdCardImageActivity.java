package com.trancis.wall.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trancis.wall.R;
import com.trancis.wall.adapters.ImageRecycleAdapter;
import com.trancis.wall.java_class.MediaModel;
import com.trancis.wall.util.ItemClickListener;
import com.trancis.wall.util.Util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by trancis on 25/8/17.
 */

public class SdCardImageActivity extends AppCompatActivity implements ItemClickListener {
    Cursor mImageCursor;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    private int mediaType;
    List<MediaModel> mGalleryModelList = new ArrayList<MediaModel>();
     final FilenameFilter IMAGE_FILTER = new FilenameFilter() {

        @Override
        public boolean accept(final File dir, final String name) {
            if (name.endsWith(".jpg")) {
                return (true);
            }

            return (false);
        }
    };

     final FilenameFilter VIDEO_FILTER = new FilenameFilter() {

        @Override
        public boolean accept(final File dir, final String name) {
            if (name.endsWith(".mp4")) {
                return (true);
            }

            return (false);
        }
    };

     final FilenameFilter GIF_FILTER = new FilenameFilter() {

        @Override
        public boolean accept(final File dir, final String name) {
            if (name.endsWith(".mp4")) {
                return (true);
            }

            return (false);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sd_card_imageview);

        mediaType = getIntent().getIntExtra("mediaType", 0);

        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar.getIndeterminateDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build();
        StrictMode.setThreadPolicy(policy);

        if (mediaType == 0)
            initPhoneImages();
        else if (mediaType == 1)
            initPhoneVideos();
        else {
            initPhoneGifs();

        }
        //getOutputMediaFileUri(MediaC.MEDIA_TYPE_IMAGE);

    }

    private void initPhoneImages() {

        File imageRecvPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/WhatsApp Images/");
        File imageSentPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/WhatsApp Images/Sent/");

        File[] fileRecvList = imageRecvPath.listFiles(IMAGE_FILTER);
        if (fileRecvList != null) {
            for (File file : fileRecvList) {
                MediaModel galleryModel = new MediaModel(file.toString(), file.lastModified());
                mGalleryModelList.add(galleryModel);
            }
        }

        File[] fileSentList = imageSentPath.listFiles(IMAGE_FILTER);
        if (fileSentList != null) {
            for (File file : fileSentList) {
                MediaModel galleryModel = new MediaModel(file.toString(), file.lastModified());
                mGalleryModelList.add(galleryModel);
            }
        }
        if (mGalleryModelList.size() > 0) {
            Util.sortListByLong(mGalleryModelList);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
            // gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
            ImageRecycleAdapter imageRecycleAdapter = new ImageRecycleAdapter(mGalleryModelList, getApplicationContext());
            recyclerView.setLayoutManager(gridLayoutManager);
            imageRecycleAdapter.setClickListener(this);
            imageRecycleAdapter.setMediaType(0);
            recyclerView.setAdapter(imageRecycleAdapter);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setAlpha(0.4f);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    recyclerView.setAlpha(1);
                    progressBar.setVisibility(View.GONE);

                }
            }, 2000);
        } else {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "No Files are available.", Toast.LENGTH_SHORT).show();
            finish();
        }
       /* try {
            final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
            final String[] columns = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID};
            mImageCursor = getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
                    null, null, orderBy + " DESC");

            for (int i = 0; i < mImageCursor.getCount(); i++) {
                mImageCursor.moveToPosition(i);
                int dataColumnIndex = mImageCursor
                        .getColumnIndex(MediaStore.Images.Media.DATA);
                MediaModel galleryModel = new MediaModel(mImageCursor.getString(
                        dataColumnIndex).toString(), 1);
                mGalleryModelList.add(galleryModel);
            }
            int a=10;
            int b=11;
            b=a;
            a=b;


            // setAdapter(mImageCursor);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void initPhoneVideos() {
        File imageRecvPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/WhatsApp Video/");
        File imageSentPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/WhatsApp Video/Sent/");

        File[] fileRecvList = imageRecvPath.listFiles(VIDEO_FILTER);
        if (fileRecvList != null) {
            for (File file : fileRecvList) {
                MediaModel galleryModel = new MediaModel(file.toString(), file.lastModified());
                mGalleryModelList.add(galleryModel);
            }
        }

        File[] fileSentList = imageSentPath.listFiles(VIDEO_FILTER);
        if (fileSentList != null) {
            for (File file : fileSentList) {
                MediaModel galleryModel = new MediaModel(file.toString(), file.lastModified());
                mGalleryModelList.add(galleryModel);
            }
        }
        if (mGalleryModelList.size() > 0) {
            Util.sortListByLong(mGalleryModelList);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
            // gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
            ImageRecycleAdapter imageRecycleAdapter = new ImageRecycleAdapter(mGalleryModelList, getApplicationContext());
            recyclerView.setLayoutManager(gridLayoutManager);
            imageRecycleAdapter.setClickListener(this);
            imageRecycleAdapter.setMediaType(1);
            recyclerView.setAdapter(imageRecycleAdapter);
            recyclerView.setAlpha(0.4f);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    recyclerView.setAlpha(1);
                    progressBar.setVisibility(View.GONE);

                }
            }, 3000);
        } else {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "No Files are available.", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    private void initPhoneGifs() {
        File imageRecvPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/WhatsApp Animated Gifs/");
        File imageSentPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/WhatsApp Animated Gifs/Sent/");

        File[] fileRecvList = imageRecvPath.listFiles(GIF_FILTER);
        if (fileRecvList != null) {
            for (File file : fileRecvList) {
                MediaModel galleryModel = new MediaModel(file.toString(), file.lastModified());
                mGalleryModelList.add(galleryModel);
            }
        }

        File[] fileSentList = imageSentPath.listFiles(GIF_FILTER);
        if (fileSentList != null) {
            for (File file : fileSentList) {
                MediaModel galleryModel = new MediaModel(file.toString(), file.lastModified());
                mGalleryModelList.add(galleryModel);
            }
        }
        if (mGalleryModelList.size() > 0) {
            Util.sortListByLong(mGalleryModelList);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
            // gridLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
            ImageRecycleAdapter imageRecycleAdapter = new ImageRecycleAdapter(mGalleryModelList, getApplicationContext());
            recyclerView.setLayoutManager(gridLayoutManager);
            imageRecycleAdapter.setClickListener(this);
            imageRecycleAdapter.setMediaType(1);
            recyclerView.setAdapter(imageRecycleAdapter);
            recyclerView.setAlpha(0.4f);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    recyclerView.setAlpha(1);
                    progressBar.setVisibility(View.GONE);

                }
            }, 3000);
        } else {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "No Files are available.", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    public void onItemClick(View view, int position) {
        if (mediaType == 0) {
            Intent mIntent = new Intent(this, ViewImageActivity.class);
            mIntent.putExtra("title", "WhatsApp Images");
            mIntent.putExtra("position", position);
            Bundle bundle = new Bundle();
            //bundle.putParcelable("lists", (Serializable)mGalleryModelList);
            //mIntent.putExtras(bundle);
            mIntent.putExtra("lists", (Serializable) mGalleryModelList);
            // mIntent.putParcelableArrayListExtra("list", (ArrayList<? extends Parcelable>) mGalleryModelList);
            startActivity(mIntent);
        } else if (mediaType == 1) {
            Intent mIntent = new Intent(this, VideoViewActivity.class);
            mIntent.putExtra("title", "WhatsApp Video");
            mIntent.putExtra("mediaType", mediaType);
            mIntent.putExtra("path", mGalleryModelList.get(position).url);

            startActivity(mIntent);
        } else {
            Intent mIntent = new Intent(this, VideoViewActivity.class);
            mIntent.putExtra("title", "WhatsApp Gifs");
            mIntent.putExtra("mediaType", mediaType);
            mIntent.putExtra("path", mGalleryModelList.get(position).url);
            startActivity(mIntent);
        }


    }

    @Override
    public void onItemLongClick(View view, int position) {

    }
}
