package com.trancis.wall.activity;

import android.graphics.Bitmap;

/**
 * Created by mohit on 05-07-2017.
 */
public class DataInfoAll
{
    int id;
    int c_id;
    private String thumbUrl;
    String realUrl;
    public Bitmap bitmap;
    public boolean isDownloading;

    public DataInfoAll(int a_id,int a_cid, String rUrl, String tUrl) {
        id = a_id;
        c_id = a_cid;
        thumbUrl = tUrl;
        realUrl = rUrl;
    }

    public String getThumbUrl() {
        return  thumbUrl;
    }

    public Integer getWallpaperId() {
        return  id;
    }

    public Integer getCatid() {
        return  c_id;
    }

    public String getRealUrl() {
        return  realUrl;
    }
}
