package com.trancis.wall.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.trancis.wall.R;


public class Password_recovery extends AppCompatActivity implements View.OnClickListener {
    EditText t14;
    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);
        b1= (Button) findViewById(R.id.button6);
        t14= (EditText) findViewById(R.id.editText14);
        b1.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public boolean onSupportNavigateUp()
    {
        this.finish();
        return true;
    }

    @Override
    public void onClick(View v)
    {
        String a = t14.getText().toString();
        Intent i_code=new Intent(this,Recovery_done.class);
        startActivity(i_code);
    }
}
