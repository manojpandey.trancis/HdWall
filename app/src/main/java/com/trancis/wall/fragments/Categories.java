package com.trancis.wall.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.trancis.wall.R;
import com.trancis.wall.activity.BaseAct_Categories;
import com.trancis.wall.adapters.ImageAdapter;
import com.trancis.wall.java_class.DataInfoCat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class Categories extends Fragment
{
    ProgressBar progressBar;
    JSONObject jsonobject;
    JSONArray jsonarray;
    GridView gridView;
    Handler handler;
    ImageAdapter adapter;
    int a = 1;
    List<DataInfoCat> dataListCat = new ArrayList<>();

    public Categories() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_categories, container, false);

        getActivity().setTitle("Categories");
        progressBar = (ProgressBar) v.findViewById(R.id.progressbarcategory);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFF2c9bf4, android.graphics.PorterDuff.Mode.MULTIPLY);

        handler = new Handler();

        gridView = (GridView) v.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                DataInfoCat dataInfoCat = dataListCat.get(position);
                Intent intent_single = new Intent(getContext(), BaseAct_Categories.class);
                intent_single.putExtra("cid", dataInfoCat.getId());
                intent_single.putExtra("category_name", dataInfoCat.getCategoryname());
                startActivity(intent_single);
                //isOnline();
            }
        });


        Runnable runnable = new Runnable() {
            public void run() {
                OkHttpClient client = new OkHttpClient();
                client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
                client.setReadTimeout(15, TimeUnit.SECONDS);

                Request request = new Request.Builder()
                        .url("http://www.viaviweb.in/envato/cc/hd_wallpaper_demo/api.php?cat_list")
                        .build();
                Call call;

                call = client.newCall(request);
                if (call != null) {
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e)
                        {
                            ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

                            if (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), "Slow Internet Connection", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            parseResponse(response);
                        }
                    });
                }
            }
        };
        runnable.run();
        return  v;
    }
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("OOPS");
            builder.setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().finish();
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

            return false;
        }
        return true;
    }
    private void parseResponse(Response response) {
        String jsonData = null;

        try {
            jsonData = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            jsonobject = new JSONObject(jsonData);
            jsonarray = jsonobject.getJSONArray("HD_WALLPAPER");
            for (int i = 0; i < jsonarray.length(); i++) {
                jsonobject = jsonarray.getJSONObject(i);
                DataInfoCat dataInfoCat = new DataInfoCat(jsonobject.getInt("cid"), jsonobject.getString("category_image_thumb"), jsonobject.getString("category_image"),jsonobject.getString("category_name"),jsonobject.getInt("total_wallpaper"));
                dataListCat.add(dataInfoCat);
            }

            updateAdapter();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateAdapter() {
        //    adapter = new ImageAdapter_Home(getActivity(), dataListHome);
        // Set the adapter to the ListView
        //  gridView.setAdapter(adapter);
        Runnable runnable = new Runnable() {
            public void run() {
                // Toast.makeText(getActivity(), "qwert", Toast.LENGTH_LONG).show();
                // Pass the results into ListViewAdapter.java
                progressBar.setVisibility(View.GONE);
                adapter = new ImageAdapter(getActivity(),R.layout.gridviewitemcategory, dataListCat);
                // Set the adapter to the ListView
                gridView.setAdapter(adapter);
            }
        };
        handler.post(runnable);
    }
}
