package com.trancis.wall.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.trancis.wall.R;
import com.trancis.wall.activity.Change_password;
import com.trancis.wall.activity.MainActivity;
import com.trancis.wall.java_class.DataBaseHelper;
import com.trancis.wall.java_class.ImageFilePath;
import com.trancis.wall.java_class.SessionManager;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
*/
public class Updated_Profile extends Fragment implements View.OnClickListener {
    EditText etFName,etLName,etEmail;
    SessionManager session;
    TextView tv_Changepass;
    MenuItem b_edit,b_save;
    String abc;
    CircleImageView prof_image,prof_imagebig;
    private static final int RESULT_LOAD_IMAGE=1;

    public Updated_Profile() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_updated__profile, container, false);

        getActivity().setTitle("Profile");

        isOnline();

        etFName=(EditText) v.findViewById(R.id.etFirstName);

        prof_image= (CircleImageView) v.findViewById(R.id.profile_image);
        prof_imagebig= (CircleImageView) v.findViewById(R.id.profile_imageBig);

        etLName=(EditText) v.findViewById(R.id.etLastName);
        etEmail=(EditText) v.findViewById(R.id.etEmail);
        tv_Changepass= (TextView) v.findViewById(R.id.tvChangePass);
        //b_edit= (MenuItem) v.findViewById(R.id.action_edit);
        //b_edit.setOnClickListener(this);
        //b_save= (MenuItem) v.findViewById(R.id.action_save);
        //b_save.setOnClickListener(this);
        tv_Changepass.setOnClickListener(this);
        prof_image.setOnClickListener(this);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        session = new SessionManager(getActivity());

        etEmail.setOnClickListener(this);
//        b4= (Button) v.findViewById(R.id.button4);
  //      b4.setOnClickListener(this);

        // Session class instance
        // get user data from session
        String key = session.getUserDetails();
        String hide = session.changepassword();
        if(hide!=null)
        {
           tv_Changepass.setVisibility(View.GONE);
        }

        updateProfileImage();

        DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
        SQLiteDatabase db = dh.getReadableDatabase();

        String query = "Select * from Login where emailid=?";
        Cursor cursor = db.rawQuery(query, new String[]{key});

         if(cursor.moveToFirst())
         {
             String fname=cursor.getString(0);
             String lname=cursor.getString(1);
             etFName.setText(fname);
             etLName.setText(lname);
         }

        etEmail.setText(key);

        abc=etEmail.getText().toString();
        etEmail.setFocusable(false);
        etLName.setFocusable(false);
        etFName.setFocusable(false);

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.update_profile_menu, menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                View v = getActivity().findViewById(R.id.action_edit);
                if (v != null) {
                    v.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            return false;
                        }
                    });
                }
                View v2 = getActivity().findViewById(R.id.action_save);
                if (v2 != null) {
                    v2.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v2) {
                            return false;
                        }
                    });
                }
            }
        });
        b_save = menu.findItem(R.id.action_save);
        b_save.setVisible(false);
        b_edit = menu.findItem(R.id.action_edit);

        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_edit:

                etLName.setFocusableInTouchMode(true);
                etFName.setFocusableInTouchMode(true);
                etFName.requestFocus();
                etFName.setSelection(etFName.getText().length());
                etLName.setSelection(etLName.getText().length());

                b_edit.setVisible(false);
                b_save.setVisible(true);

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        View v = getActivity().findViewById(R.id.action_edit);
                        if (v != null) {
                            v.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {
                                    return false;
                                }
                            });
                        }
                        View v2 = getActivity().findViewById(R.id.action_save);
                        if (v2 != null) {
                            v2.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v2) {
                                    return false;
                                }
                            });
                        }
                    }
                });

                // search action
                return true;
            case R.id.action_save:

                if(etFName.getText().toString().trim().contains (" ")) {
                    Toast.makeText(getActivity(),"Spaces are not allowed in First Name", Toast.LENGTH_LONG).show();
                }

                else if(etLName.getText().toString().trim().contains (" ")) {
                    Toast.makeText(getActivity(),"Spaces are not allowed in Last Name", Toast.LENGTH_LONG).show();
                }

                else if(etFName.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(getActivity(), "Please Enter Your First Name", Toast.LENGTH_SHORT).show();
                }

                else if(etLName.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(getActivity(), "Please Enter Your Last Name", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
                    SQLiteDatabase db = dh.getWritableDatabase();

                    ContentValues cv=new ContentValues();
                    cv.put("fname",etFName.getText().toString());
                    cv.put("lname",etLName.getText().toString());
                    //cv.put("emailid",t3.getText().toString());


                    int ans=db.update("Login",cv,"emailid=?",new String[]{abc});

                    if(ans!=0)
                    {
                        Toast.makeText(getActivity(), "Successfully Updated", Toast.LENGTH_SHORT).show();
                        b_edit.setVisible(true);
                        b_save.setVisible(false);
                        etLName.setFocusable(false);
                        etFName.setFocusable(false);
                        InputMethodManager inputManager = (InputMethodManager) getActivity().
                                getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Not Updated", Toast.LENGTH_SHORT).show();
                    }
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            View v = getActivity().findViewById(R.id.action_edit);
                            if (v != null) {
                                v.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        return false;
                                    }
                                });
                            }
                            View v2 = getActivity().findViewById(R.id.action_save);
                            if (v2 != null) {
                                v2.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v2) {
                                        return false;
                                    }
                                });
                            }
                        }
                    });
                }
                // location found
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v)
    {
         if(v.getId()==R.id.tvChangePass) {
            Intent i_pass = new Intent(getActivity(), Change_password.class);
            startActivity(i_pass);
        }
        else if(v.getId()==R.id.profile_image)
        {
             Intent galleryIntent=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
             startActivityForResult(galleryIntent,RESULT_LOAD_IMAGE);
        }
         /*else if(v.getId()==R.id.button_edit)
         {
             t2.setFocusableInTouchMode(true);
             t1.setFocusableInTouchMode(true);
             t1.requestFocus();
             b_edit.setVisibility(View.GONE);
             b_save.setVisibility(View.VISIBLE);
         }
         else if(v.getId()==R.id.button_save)
         {
             if(t1.getText().toString().isEmpty() || t2.getText().toString().isEmpty())
             {
                 Toast.makeText(getActivity(), "Please Enter Your FullName", Toast.LENGTH_LONG).show();
             }
             else
             {
                 DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
                 SQLiteDatabase db = dh.getWritableDatabase();

                 ContentValues cv=new ContentValues();
                 cv.put("fname",t1.getText().toString());
                 cv.put("lname",t2.getText().toString());
                 //cv.put("emailid",t3.getText().toString());


                 int ans=db.update("Login",cv,"emailid=?",new String[]{abc});

                 if(ans==1)
                 {
                     Toast.makeText(getActivity(), "Successfully Updated", Toast.LENGTH_LONG).show();
                     b_edit.setVisibility(View.VISIBLE);
                     b_save.setVisibility(View.GONE);
                     t2.setFocusable(false);
                     t1.setFocusable(false);
                 }
                 else
                 {
                     Toast.makeText(getActivity(), "Not Updated", Toast.LENGTH_LONG).show();
                 }
             }
                 *//**//*DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
                 SQLiteDatabase db = dh.getWritableDatabase();

                 ContentValues cv=new ContentValues();
                 cv.put("fname",t1.getText().toString());
                 cv.put("lname",t2.getText().toString());
                 //cv.put("emailid",t3.getText().toString());


                 int ans=db.update("Login",cv,"emailid=?",new String[]{abc});

                 if(ans==1)
                 {
                     Toast.makeText(getActivity(), "Successfully Updated", Toast.LENGTH_LONG).show();
                     b_edit.setVisibility(View.VISIBLE);
                     b_save.setVisibility(View.GONE);
                 }
                 else
                 {
                     Toast.makeText(getActivity(), "Not Updated", Toast.LENGTH_LONG).show();
                 }

         }*/
        else if(v.getId()==R.id.etEmail)
        {
            Toast.makeText(getActivity(), "You can not update email", Toast.LENGTH_SHORT).show();
        }
        //else if(v.getId()==R.id.button4)

    }
    public void updateProfileImage()
    {
        String uri = session.getProfileImage();
        if (uri != null)
        {
            //Toast.makeText(getContext(), uri, Toast.LENGTH_LONG).show();
                prof_imagebig.setImageURI(Uri.parse(uri));
        }
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("OOPS");
            builder.setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().finish();
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && data!=null)
        {
            Uri selected_image = data.getData();

            File nFile= new File("/storage/0EA5-63A9/Manoj4/img.jpg");
            File nFile2= new File("/storage/emulated/0/Manoj/img.jpg");
            File nFile3= new File( Environment.getExternalStorageDirectory().getAbsolutePath()+"/Manoj/img.jpg");

            if(nFile.exists()){
                int a=10;
                int b=11;
                b=a;
                a=b;
            }
            if(nFile2.exists()){
                int a=10;
                int b=11;
                b=a;
                a=b;
            }
            if(nFile3.exists()){
                int a=10;
                int b=11;
                b=a;
                a=b;
            }
            String path=ImageFilePath.getPath(getActivity(),selected_image);
            String sdcardPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Manoj4/img.jpg";
            //Toast.makeText(getActivity(), String.valueOf(selected_image), Toast.LENGTH_LONG).show();
            prof_imagebig.setImageURI(selected_image);
            ((MainActivity)getActivity()).setDrawerImage(data);
            session.createProfileImage(selected_image);
        }
    }
}
