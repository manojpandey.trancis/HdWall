package com.trancis.wall.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.trancis.wall.R;




/**
 * A simple {@link Fragment} subclass.
 */
public class Privacy_Dialog extends Fragment {

    TextView lp;
    public Privacy_Dialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_privacy__dialog, container, false);

        lp=(TextView) v.findViewById(R.id.textView4);
        //lp.setText(Html.fromHtml(<html> This Is Pricacy Policy </html>));
        lp.setText(Html.fromHtml("<h1><u>Privacy Policy</u></h1><br><p><h2>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<p></p> <h3>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.,</p><p> <h3>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></br></h3></h2></p>"));


        return v;
    }

}
