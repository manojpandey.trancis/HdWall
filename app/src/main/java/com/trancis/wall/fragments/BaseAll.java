package com.trancis.wall.fragments;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.trancis.wall.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class BaseAll extends Fragment {


    public BaseAll() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base_all, container, false);
    }

}
