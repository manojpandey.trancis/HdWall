package com.trancis.wall.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.trancis.wall.R;
import com.trancis.wall.activity.MainActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Rating extends Fragment implements View.OnClickListener {

    RatingBar r;
    Button b3;
    EditText et;

    public Fragment_Rating() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_fragment__rating, container, false);

        getActivity().setTitle("Rate Us");

        isOnline();

        r= (RatingBar) v.findViewById(R.id.ratingBar);
        /*Drawable progress = r.getProgressDrawable();
        DrawableCompat.setTint(progress, Color.WHITE);
   */     et = (EditText) v.findViewById(R.id.editText7);
        b3= (Button) v.findViewById(R.id.button3);
        b3.setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("OOPS");
            builder.setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().finish();
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v)
    {
       if(v.getId()==R.id.button3)
       {
           String rating = String.valueOf(r.getRating());
           String x = et.getText().toString();
           if (x.isEmpty() && r.getRating() == 0.0) {
               AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

               builder.setMessage("Please give Rating and Feedback").setCancelable(false)
                       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                               dialog.dismiss();
                           }
                       });


               AlertDialog alert = builder.create();

               //Display the message!
               alert.show();
               Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
               nbutton.setTextColor(Color.BLACK);
               Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
               pbutton.setTextColor(Color.BLACK);

           }
           else if(x.isEmpty())
           {
               AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

               builder.setMessage("Please Write your Review").setCancelable(false)
                       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                               dialog.dismiss();
                           }
                       });


               AlertDialog alert = builder.create();

               //Display the message!
               alert.show();
               Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
               nbutton.setTextColor(Color.BLACK);
               Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
               pbutton.setTextColor(Color.BLACK);

           }
           else if(r.getRating() == 0.0)
           {
               AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

               builder.setMessage("Please give us Rating").setCancelable(false)
                       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                               dialog.dismiss();
                           }
                       });

               AlertDialog alert = builder.create();

               //Display the message!
               alert.show();
               Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
               nbutton.setTextColor(Color.BLACK);
               Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
               pbutton.setTextColor(Color.BLACK);

           }

           else
           {
               AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

               builder.setTitle("Thank You");

               builder.setMessage("We appreciate your review " + rating + ". We will work hard upon it.").setCancelable(false)
                       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                               dialog.dismiss();
                               Intent newIntent = new Intent(getContext(), MainActivity.class);
                               startActivity(newIntent);
                           }
                       });

               AlertDialog alert = builder.create();

               //Display the message!
               alert.show();
               Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
               nbutton.setTextColor(Color.BLACK);
               Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
               pbutton.setTextColor(Color.BLACK);
               et.setText("");
           }
       }
    }
}
