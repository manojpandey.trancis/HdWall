package com.trancis.wall.fragments;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.trancis.wall.R;
import com.trancis.wall.activity.Main2Activity;
import com.trancis.wall.activity.MainActivity;
import com.trancis.wall.java_class.DataBaseHelper;
import com.trancis.wall.java_class.SessionManager;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by mohit on 07-06-2017.
 */
public class Signup extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    EditText et_Fname, et_Lname, et_Email, et_Pass;
    Button btnSignup;

    LinearLayout linearLayout;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String fname, lname, emailid;
    static int changepassword = 0;
    SignInButton signinbtn;
    LoginButton fab_login;
    public static CallbackManager callbackManager;
    //ProgressDialog asyncDialog = new ProgressDialog(getActivity());
    //public static int CODE = 9999;
    public static GoogleApiClient googleApiClient;
    public static int REQ_CODE = 9999;
    SessionManager sessionManager;
    ProgressDialog progress;
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getContext());
        View rootView = inflater.inflate(R.layout.signup, container, false);
        sessionManager = new SessionManager(getContext());
        //return rootView;

        ((Main2Activity) getActivity()).mViewPager.setCurrentItem(1);

        progress=new ProgressDialog(getActivity());
        progress.setMessage("Logging In");
        progress.setIndeterminate(false);
        progress.setCancelable(false);


        et_Fname = (EditText) rootView.findViewById(R.id.et_FirstName);
        et_Lname = (EditText) rootView.findViewById(R.id.et_LastName);
        et_Email = (EditText) rootView.findViewById(R.id.et_EmailId);
        et_Pass = (EditText) rootView.findViewById(R.id.et_Password);
        et_Pass.setTypeface(et_Email.getTypeface());

        fab_login = (LoginButton) rootView.findViewById(R.id.fb_login);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);

        fab_login.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));

        callbackManager = CallbackManager.Factory.create();
        fab_login.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
        {
            private ProfileTracker mProfileTracker;

            @Override
            public void onSuccess(LoginResult loginResult)
            {
                fetchFbInfo(loginResult);
                /*if(Profile.getCurrentProfile() == null) {
                    mProfileTracker = new ProfileTracker() {


                        @Override
                        protected void onCurrentProfileChanged(Profile profile, Profile profile2) {


                            // profile2 is the new profile
                            //emailid = profile2.;
                            fname = profile2.getFirstName();
                            lname = profile2.getLastName();

                            Toast.makeText(getActivity(),"Yoo man your "+emailid+""+fname+""+lname, Toast.LENGTH_LONG).show();
                            mProfileTracker.stopTracking();
                        }
                    };
                    // no need to call startTracking() on mProfileTracker
                    // because it is called by its constructor, internally.
                }
                else {
                    Profile profile = Profile.getCurrentProfile();
                    //Toast.makeText(getActivity(), profile.getFirstName(), Toast.LENGTH_LONG).show();
                }*/
                /*AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                if(profile != null){
                    emailid = profile.getFirstName();

                }else{
                    mProfileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                            // profile2 is the new profile
                            emailid = profile2.getName();
                            mProfileTracker.stopTracking();
                        }
                    };

                }

                Toast.makeText(getActivity(), emailid , Toast.LENGTH_LONG).show();
*/

                /*progress.show();
                Profile profile = Profile.getCurrentProfile();

                    if (profile != null)
                    {
                        emailid=profile.getId();
                        fname=profile.getFirstName();
                        lname=profile.getLastName();
                        Toast.makeText(getActivity(), emailid+" "+ fname+" "+lname+" ", Toast.LENGTH_LONG).show();
                    }
*/
                Intent i_main = new Intent(getActivity(), MainActivity.class);
                //i_main.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                //i_main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                sessionManager.setLogin(true);
               // Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
               // i_main.putExtras(Bundle parameters);
                startActivity(i_main);
                getActivity().finish();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getActivity(), "Sorry Try Manual Login", Toast.LENGTH_LONG).show();
                sessionManager.setLogin(false);
            }

            @Override
            public void onError(FacebookException error)
            {
                String str = error.getLocalizedMessage();
                sessionManager.setLogin(false);

            }
        });

        signinbtn = (SignInButton) rootView.findViewById(R.id.googleBtn);
        signinbtn.setOnClickListener(this);
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        googleApiClient = new GoogleApiClient.Builder(getActivity()).enableAutoManage(getActivity(), this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();

       linearLayout = (LinearLayout) rootView.findViewById(R.id.layoutofgooglebtn);
       linearLayout.setOnClickListener(this);

        btnSignup = (Button) rootView.findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(this);
        return rootView;
    }

    private void fetchFbInfo(LoginResult loginResult)
    {
        if (loginResult != null) {
            AccessToken accessToken  = loginResult.getAccessToken();
            GraphRequest graphRequest =GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response)
                {

                    try {
                        /*
                            {
                                "last_name": "Sharma",
                                    "id": "108194416459958",
                                    "email": "kk0000sharma@gmail.com",
                                    "first_name": "Mohit"
                            }
                        */
                           emailid = object.getString("email");
                           fname = object.getString("first_name");
                           lname = object.getString("last_name");

                        DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
                        SQLiteDatabase db = dh.getWritableDatabase();

                        ContentValues cv = new ContentValues();
                        cv.put("fname", fname);
                        cv.put("lname", lname);
                        cv.put("emailid", emailid);
                        cv.put("password",fname);

                        db.insert("Login", null, cv);

                        sessionManager.createLoginSession(emailid);

                        String hide = "this is to hide button";//to hide change password
                        sessionManager.setChangepassword(hide);//to hide change password

                        Toast.makeText(getActivity(),"You are Logged In as "+emailid, Toast.LENGTH_LONG).show();
                        }
                    catch(Exception e)
                    {

                    }
                }
            });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email");
            graphRequest.setParameters(parameters);
            graphRequest.executeAsync();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.googleBtn)
        {
            signIn();
        }
        else if (v.getId() == R.id.layoutofgooglebtn)
        {
            signIn();
        }
        else if (v.getId() == R.id.btnSignup)
        {
            String w = et_Fname.getText().toString();
            String x = et_Lname.getText().toString();
            String y = et_Email.getText().toString();
            String z = et_Pass.getText().toString();

            if (x.isEmpty() && y.isEmpty() && w.isEmpty() && z.isEmpty()) {
                Toast.makeText(getActivity(), "Please Fill all Fields", Toast.LENGTH_LONG).show();
            }
            else if(w.isEmpty() && x.isEmpty())
            {
                Toast.makeText(getActivity(),"Please Enter Your Fullname",Toast.LENGTH_LONG).show();
            }
            else if(w.isEmpty())
            {
                Toast.makeText(getActivity(),"First Name can't be left blank",Toast.LENGTH_LONG).show();
            }
            else if(y.isEmpty())
            {
                Toast.makeText(getActivity(),"Please enter your Email",Toast.LENGTH_LONG).show();
            }
            else if(z.isEmpty())
            {
                Toast.makeText(getActivity(),"Please Enter the Password",Toast.LENGTH_LONG).show();
            }
            else {

                if (y.matches(emailPattern) && et_Email.length() > 0)
                {
                    DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
                    SQLiteDatabase db = dh.getWritableDatabase();

                    ContentValues cv = new ContentValues();
                    cv.put("fname", w);
                    cv.put("lname", x);
                    cv.put("emailid", y);
                    cv.put("password", z);

                    db.insert("Login", null, cv);

                    Toast.makeText(getActivity(), "successfully signed up", Toast.LENGTH_LONG).show();

                    ((Main2Activity) getActivity()).mViewPager.setCurrentItem(1);
                    et_Fname.setText("");
                    et_Lname.setText("");
                    et_Email.setText("");
                    et_Pass.setText("");
                }
                else
                {
                    Toast.makeText(getContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }
/*
                DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
                SQLiteDatabase db = dh.getWritableDatabase();


                ContentValues cv = new ContentValues();
                cv.put("fname", w);
                cv.put("lname", x);
                cv.put("emailid", y);
                cv.put("password", z);

                db.insert("Login", null, cv);

                Toast.makeText(getActivity(), "successfully signed up", Toast.LENGTH_LONG).show();

                ((Main2Activity) getActivity()).mViewPager.setCurrentItem(1);
                t1.setText("");
                t2.setText("");
                t3.setText("");
                t4.setText("");
*/
            }

        }
    }

    private void signIn() {

        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, REQ_CODE);
        //show dialog
        progress.show();

    }

    public void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
             emailid = account.getEmail();
             fname = account.getGivenName();
             lname = account.getFamilyName();

            //Toast.makeText(getActivity(), "You are logged in as " + user_email, Toast.LENGTH_LONG).show();
            //Toast.makeText(getActivity(), fname, Toast.LENGTH_LONG).show();
            Toast.makeText(getActivity(), "You are logged in as " +emailid, Toast.LENGTH_LONG).show();

            DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
            SQLiteDatabase db = dh.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("fname", fname);
            cv.put("lname", lname);
            cv.put("emailid", emailid);
            cv.put("password",fname);

            db.insert("Login", null, cv);

            sessionManager.createLoginSession(emailid);

            updateUI(true);
        } else {
            updateUI(false);
        }
    }

    public void updateUI(boolean isLogin) {
        if (isLogin) {
           String hide = "this is to hide button";//to hide change password
           sessionManager.setChangepassword(hide);//to hide change password
           progress.dismiss();
            Intent i_main = new Intent(getActivity(), MainActivity.class);
            //i_main.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            //i_main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            sessionManager.setLogin(true);
            getActivity().finish();
            startActivity(i_main);

        } else {
            Toast.makeText(getActivity(), "Sorry try manual login", Toast.LENGTH_LONG).show();
            progress.dismiss();

              /*Intent i_main=new Intent(getActivity(),MainActivity.class);
              i_main.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
              //i_main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
              startActivity(i_main);
              getActivity().finish();*/
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
        //callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
