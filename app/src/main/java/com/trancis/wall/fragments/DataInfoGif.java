package com.trancis.wall.fragments;

import android.graphics.Bitmap;

/**
 * Created by mohit on 03-07-2017.
 */
public class DataInfoGif
{
    int id;
    public String realUrl;
    public Bitmap bitmap;
    public boolean isDownloading;

    public DataInfoGif(int id, String rUrl) {
        this.id = id;
        realUrl = rUrl;
    }
    public String getRealUrl() {
        return  realUrl;
    }
}
