package com.trancis.wall.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.trancis.wall.R;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.trancis.wall.activity.GifActivity;
import com.trancis.wall.adapters.ImageAdapter_gif;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 */
public class Gifs extends Fragment
{
    JSONObject jsonobject;
    JSONArray jsonarray;
    GridView gridView;
    Handler handler;
    ImageAdapter_gif adapter;
    List<DataInfoGif> dataListGif = new ArrayList<>();
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gifs, container, false);

        getActivity().setTitle("GIFs");
        progressBar = (ProgressBar) v.findViewById(R.id.progressbargifs);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.getIndeterminateDrawable().setColorFilter(0xFF2c9bf4, android.graphics.PorterDuff.Mode.MULTIPLY);

        isOnline();

        handler = new Handler();
        gridView = (GridView) v.findViewById(R.id.gridViewgif);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DataInfoGif dataInfoGif = dataListGif.get(position);

                Intent intent_singlegif = new Intent(getActivity(), GifActivity.class);
                intent_singlegif.putExtra("url", dataInfoGif.getRealUrl());
                startActivity(intent_singlegif);
                List<String> urlList=new ArrayList<String>();

            }
        });


        Runnable runnable = new Runnable() {
            public void run() {
                OkHttpClient client = new OkHttpClient();
                client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
                client.setReadTimeout(15, TimeUnit.SECONDS);
                Request request = new Request.Builder()
                        .url("http://www.viaviweb.in/envato/cc/hd_wallpaper_demo/api.php?gif_list")
                        .build();
                Call call;

                call = client.newCall(request);
                if (call != null)
                {
                    call.enqueue(new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
                            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

                            if (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()) {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), "Slow Internet Connection", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            parseResponse(response);
                        }
                    });
                }
            }
        };
        runnable.run();
        return v;
    }

    private void parseResponse(Response response) {
        String jsonData = null;


        try {
            jsonData = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            jsonobject = new JSONObject(jsonData);
            jsonarray = jsonobject.getJSONArray("HD_WALLPAPER");
            for (int i = 0; i < jsonarray.length(); i++)
            {
                jsonobject = jsonarray.getJSONObject(i);
                DataInfoGif dataInfoGif = new DataInfoGif(jsonobject.getInt("id"), jsonobject.getString("gif_image"));
                dataListGif.add(dataInfoGif);
            }

            updateAdapter();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("OOPS");
            builder.setMessage("No Internet Connection")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            getActivity().finish();
                        }
                    })
            ;
            AlertDialog alert = builder.create();
            alert.show();
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(Color.BLACK);
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(Color.BLACK);

            return false;
        }
        return true;
    }
    private void updateAdapter() {
        //    adapter = new ImageAdapter_Home(getActivity(), dataListHome);
        // Set the adapter to the ListView
        //  gridView.setAdapter(adapter);
        Runnable runnable = new Runnable() {
            public void run() {
                // Toast.makeText(getActivity(), "qwert", Toast.LENGTH_LONG).show();

                // Pass the results into ListViewAdapter.java
                progressBar.setVisibility(View.GONE);
                adapter = new ImageAdapter_gif(getActivity(),R.layout.gridview_item, dataListGif);
                // Set the adapter to the ListView
                gridView.setAdapter(adapter);
            }
        };
        handler.post(runnable);
    }
}
