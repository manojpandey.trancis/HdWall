package com.trancis.wall.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.trancis.wall.R;
import com.trancis.wall.activity.SdCardImageActivity;

/**
 * Created by trancis on 24/8/17.
 */

public class WhatsAppGallary extends Fragment {

    FrameLayout frameImage, frameVideo, frameGif;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.whatsapp_gallary, container, false);
        getActivity().setTitle("WhatsApp");
        frameImage=(FrameLayout)view.findViewById(R.id.frameImage);
        frameGif=(FrameLayout)view.findViewById(R.id.frameGif);
        frameVideo=(FrameLayout)view.findViewById(R.id.frameVideo);
        frameImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), SdCardImageActivity.class);
                intent.putExtra("mediaType",0);
                startActivity(intent);
            }
        });
        frameVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), SdCardImageActivity.class);
                intent.putExtra("mediaType",1);
                startActivity(intent);
            }
        });
        frameGif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), SdCardImageActivity.class);
                intent.putExtra("mediaType",2);
                startActivity(intent);
            }
        });
        return view;
    }
}
