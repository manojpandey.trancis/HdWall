package com.trancis.wall.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.trancis.wall.R;

import com.trancis.wall.activity.MainActivity;
import com.trancis.wall.activity.Password_recovery;
import com.trancis.wall.java_class.DataBaseHelper;
import com.trancis.wall.java_class.SessionManager;


/**
 * Created by mohit on 07-06-2017.
 */
public class Login extends Fragment implements View.OnClickListener {
    EditText et_Username,et_PassWord;
    Button btn_Login;
    TextView tv_Forgetpass;

    SessionManager session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
         View rootView = inflater.inflate(R.layout.login, container, false);
         return rootView;
    }

    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        et_Username = (EditText) getActivity().findViewById(R.id.et_Username);
        et_PassWord = (EditText) getActivity().findViewById(R.id.et_Passcode);
        tv_Forgetpass= (TextView) getActivity().findViewById(R.id.tv_ForgetPass);
        tv_Forgetpass.setOnClickListener(this);
        btn_Login = (Button) getActivity().findViewById(R.id.btn_Login);
        btn_Login.setOnClickListener(this);
        session = new SessionManager(getActivity());
        et_PassWord.setTypeface(et_Username.getTypeface());
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId()==R.id.tv_ForgetPass)
        {
            Intent i_pass_recovery = new Intent(getActivity(), Password_recovery.class);
            startActivity(i_pass_recovery);
        }
        else if(v.getId()==R.id.btn_Login)
        {
            String emailid = et_Username.getText().toString();
            String password = et_PassWord.getText().toString();

            if(emailid.isEmpty() && password.isEmpty())
            {
                Toast.makeText(getActivity(),"Please enter your mail and password",Toast.LENGTH_LONG).show();
            }
            else if(password.isEmpty())
            {
                Toast.makeText(getActivity(),"Please Enter your pssword",Toast.LENGTH_LONG).show();
            }
            else if(emailid.isEmpty())
            {
                Toast.makeText(getActivity(),"Please Enter the Email",Toast.LENGTH_LONG).show();
            }
            else
            {
                //String emailid = t5.getText().toString();
                //String password = t6.getText().toString();

                DataBaseHelper dh = new DataBaseHelper(getActivity().getApplicationContext());
                SQLiteDatabase db = dh.getReadableDatabase();

                String s = "select * from Login where emailid=?";
                Cursor c = db.rawQuery(s, new String[]{emailid});

                if (c.moveToFirst())
                {
                    if (c.getString(2).equals(emailid) && c.getString(3).equals(password))
                    {
                        Toast.makeText(getActivity(), "successfully login", Toast.LENGTH_LONG).show();
                        // Creating user login session
                        // For testing i am stroing email as follow
                        // Use user real data
                        session.createLoginSession(emailid);
                        Intent i = new Intent(getActivity(), MainActivity.class);
                        startActivity(i);
                        et_Username.setText("");
                        et_PassWord.setText("");
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Incorrect Credentials", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), "No Account found for"+emailid, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
